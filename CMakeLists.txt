cmake_minimum_required(VERSION 3.18)
project(commute)

include(cmake/Logging.cmake)

include(cmake/CheckCompiler.cmake)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

include(cmake/Sanitize.cmake)

option(COMMUTE_TESTS "Build commute tests" OFF)
option(COMMUTE_EXAMPLES "Build commute examples" OFF)
option(COMMUTE_DEVELOPER "Commute development mode" OFF)

add_subdirectory(third_party)

include(cmake/CompileOptions.cmake)
include(cmake/ProtobufUtil.cmake)
include(cmake/PrintDiagnostics.cmake)

add_subdirectory(commute)

if(COMMUTE_TESTS OR COMMUTE_DEVELOPER)
    add_subdirectory(tests)
    add_subdirectory(stress_tests)
endif()

if(COMMUTE_EXAMPLES OR COMMUTE_DEVELOPER)
    add_subdirectory(examples)
    add_subdirectory(sims)
endif()
