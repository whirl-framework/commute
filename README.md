# Commute

Fault-tolerant RPC framework with pluggable runtime

## Examples

- [HelloWorld](examples/hello/main.cpp)
- [Tracing](examples/trace/main.cpp)
- [Proxy](examples/proxy/main.cpp)
- [Retries](examples/retries/main.cpp)
- [Timeout / deadline propagation](examples/timeout/main.cpp)
- [Circuit-Breaker](examples/circuit_breaker/main.cpp)
- [Remote cancellation](examples/cancel/main.cpp)
- [Semaphore](examples/semaphore/main.cpp)

## Simulations

- [Backoff](sims/backoff/main.cpp)
- [Load balancing](sims/balance/main.cpp)
- [Hedging](sims/hedge/main.cpp)

## Dependencies

- [Await](https://gitlab.com/Lipovsky/await) – concurrency
- [Timber](https://gitlab.com/whirl-framework/timber) – observability
- [Snowflake](https://gitlab.com/whirl-framework/snowflake) – IDs and random numbers
- [TickTock](https://gitlab.com/whirl-framework/ticktock) – clocks

### Third-party
- [Protocol Buffers](https://github.com/google/protobuf) – serialization

## Inspiration

- [Finagle](https://twitter.github.io/finagle/)
- [Finagle 101](https://kostyukov.net/posts/finagle-101/) by Vladimir Kostyukov
- [Your Server as a Function](https://monkey.org/~marius/funsrv.pdf), [RPC Redux](https://monkey.org/~marius/redux.html) by Marius Eriksen
- [Cap'n Proto PRC Protocol](https://capnproto.org/rpc.html) by Kenton Varda

## Availability

- [Exponential Backoff And Jitter](https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/)
- [Timeouts, retries, and backoff with jitter](https://aws.amazon.com/builders-library/timeouts-retries-and-backoff-with-jitter/)
- [Fixing retries with token buckets and circuit breakers](https://brooker.co.za/blog/2022/02/28/retries.html)
- [What's the Cost of a Millisecond?](https://www.youtube.com/watch?v=JgrcaK0WQCQ)
- [Queueing Theory in Practice](https://www.youtube.com/watch?v=Hda5tMrLJqc)
- [The Tail at Scale](https://www.barroso.org/publications/TheTailAtScale.pdf)
- [Addressing Cascading Failures](https://sre.google/sre-book/addressing-cascading-failures/)

## Requirements

- Compiler: `clang++` (≥ 13)
- Operating System: Linux, MacOS
- Architecture: x86-64 / Arm-64

## Build

```shell
# Clone repo
git clone https://gitlab.com/whirl-framework/commute.git
cd commute
# Build
mkdir build && cd build
# Bootstrap / Step 1
cmake ..
# Bootstrap / Step 2: compile protobuf compiler
make protoc
# Bootstrap / Step 3
cmake -DCOMMUTE_TESTS=ON -DCOMMUTE_EXAMPLES=ON ..
# Build example
make commute_example_hello
# Run example
./examples/hello/commute_example_hello
```
