#pragma once

#include <commute/concurrency/futures/values/result.hpp>

#include <await/futures/model/consumer.hpp>

namespace commute::futures {

template <typename T>
using IConsumer = await::futures::IConsumer<fallible::Result<T>>;

}  // namespace commute::futures
