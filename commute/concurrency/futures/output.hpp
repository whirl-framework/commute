#pragma once

#include <await/futures/model/output.hpp>

#include <fallible/result/result.hpp>

namespace commute::futures {

template <typename T>
using Output = await::futures::Output<fallible::Result<T>>;

}  // namespace commute::futures
