#pragma once

#include <await/futures/impl/value/t/interrupt.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

namespace await::futures::value {

// Result<T>, Error -> Result<T>

template <typename T>
struct InterruptTraits<fallible::Result<T>, fallible::Error> {
  using Result = fallible::Result<T>;

  using ValueType = Result;

  static Result Value(Result r) {
    return std::move(r);
  }

  static Result Interrupt(fallible::Error e) {
    return fallible::Fail(std::move(e));
  }
};

}  // namespace await::futures::value
