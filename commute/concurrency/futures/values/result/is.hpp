#pragma once

#include <fallible/result/result.hpp>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsResult {
  static const bool Value = false;
};

template <typename T>
struct IsResult<fallible::Result<T>> {
  static const bool Value = true;
};

}  // namespace match

template <typename T>
concept IsResult = match::IsResult<T>::Value;

}  // namespace await::futures::value
