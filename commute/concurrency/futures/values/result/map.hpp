#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/value/t/map.hpp>

#include <commute/concurrency/futures/values/result/is.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// Result<T> -> (T -> U) -> Result<U>

template <typename T, typename F>
struct Map<fallible::Result<T>, F> {
  F f;

  // Mapper output type
  using U = std::invoke_result_t<F, T>;

  // Requirements for mapper output type
  static_assert(!IsResult<U> && !lazy::Thunk<U>);

  auto operator()(fallible::Result<T> r) -> fallible::Result<U> {
    // return std::move(r).Map(std::move(f));
    if (r.IsOk()) {
      return fallible::Ok(f(std::move(*r)));
    } else {
      return fallible::PropagateError(r);
    }
  };
};

}  // namespace apply

}  // namespace await::futures::value
