#pragma once

#include <await/futures/impl/value/t/or_else.hpp>

#include <fallible/result/result.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// Result<T> -> (Error -> Result<T>) -> Result<T>

template <typename T, typename F>
struct OrElse<fallible::Result<T>, F> {
  F f;

  // Mapper output type
  using R = std::invoke_result_t<F, fallible::Error>;

  // Requirements for mapper output type
  static_assert(std::is_same_v<R, fallible::Result<T>>);

  auto operator()(fallible::Result<T> r) -> fallible::Result<T> {
    return std::move(r).Recover(std::move(f));
  };
};

}  // namespace apply

}  // namespace await::futures::value
