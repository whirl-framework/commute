#pragma once

#include <await/futures/impl/value/t/timeout.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>
#include <fallible/error/codes.hpp>

namespace await::futures::value {

//////////////////////////////////////////////////////////////////////

template <typename E>
struct TimeoutTraits {
  // Missing
};

template <>
struct TimeoutTraits<std::error_code> {
  static std::error_code Timeout() {
    return std::make_error_code(std::errc::timed_out);
  }
};

//////////////////////////////////////////////////////////////////////

template <typename T>
struct WithTimeoutTraits<fallible::Result<T>> {
  using ResultType = fallible::Result<T>;

  static ResultType Timeout() {
    return fallible::Fail(fallible::errors::TimedOut());
  }
};

}  // namespace await::futures::value
