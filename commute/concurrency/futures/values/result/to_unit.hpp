#pragma once

#include <await/futures/impl/value/t/to_unit.hpp>

#include <wheels/core/unit.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

namespace await::futures::value {

template <typename T>
struct ToUnitTraits<fallible::Result<T>> {
  using UnitType = fallible::Result<wheels::Unit>;

  static UnitType ToUnit(fallible::Result<T> r) {
    return fallible::JustStatus(r);
  }
};

}  // namespace await::futures::value
