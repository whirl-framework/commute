#pragma once

#include <twist/ed/stdlike/atomic.hpp>

namespace commute::concurrency {

class Semaphore {
 public:
  explicit Semaphore(size_t limit)
      : permits_(limit) {
  }

  bool TryAcquire() {
    while (true) {
      size_t permits = permits_.load();

      if (permits == 0) {
        return false;
      }

      if (permits_.compare_exchange_weak(permits, permits - 1)) {
        return true;
      }
    }
  }

  void Release() {
    permits_.fetch_add(1);
  }

 private:
  twist::ed::stdlike::atomic<size_t> permits_;
};

}  // namespace commute::concurrency
