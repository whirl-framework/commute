#pragma once

#include <string>
#include <vector>

#include <commute/discovery/path.hpp>

namespace commute::discovery {

struct IDiscovery {
  virtual ~IDiscovery() = default;

  // Service -> list of host names
  virtual std::vector<std::string> Lookup(Path path) = 0;
};

}  // namespace commute::discovery
