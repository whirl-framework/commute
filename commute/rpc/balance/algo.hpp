#pragma once

#include <commute/rpc/balance/node.hpp>

#include <vector>

namespace commute::rpc::balance {

//////////////////////////////////////////////////////////////////////

using Nodes = std::vector<INodePtr>;

//////////////////////////////////////////////////////////////////////

struct IDistributor {
  virtual ~IDistributor() = default;

  virtual INodePtr Pick(const Request&) = 0;
};

using IDistributorPtr = std::shared_ptr<IDistributor>;

//////////////////////////////////////////////////////////////////////

struct IAlgorithm {
  virtual ~IAlgorithm() = default;

  virtual IDistributorPtr Make(Nodes) = 0;
};

using IAlgorithmPtr = std::shared_ptr<IAlgorithm>;

}  // namespace commute::rpc::balance
