#include <commute/rpc/balance/algo/random.hpp>

#include <commute/rt/locate/random.hpp>

namespace commute::rpc::balance::algo {

//////////////////////////////////////////////////////////////////////

class RandomDistributor final : public IDistributor {
 public:
  RandomDistributor(Nodes nodes)
    : nodes_(std::move(nodes)) {
  }

  INodePtr Pick(const Request&) override {
    return nodes_[RandomIndex()];
  }

 private:
  size_t RandomIndex() const {
    return rt::Random().Choice(nodes_.size());
  }

 private:
  const Nodes nodes_;
};

//////////////////////////////////////////////////////////////////////

struct RandomAlgorithm : IAlgorithm {
  IDistributorPtr Make(Nodes nodes) override {
    return std::make_shared<RandomDistributor>(std::move(nodes));
  }
};

//////////////////////////////////////////////////////////////////////

IAlgorithmPtr Random() {
  return std::make_shared<RandomAlgorithm>();
}

}  // namespace commute::rpc::balance::algo
