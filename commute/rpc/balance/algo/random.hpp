#pragma once

#include <commute/rpc/balance/algo.hpp>

namespace commute::rpc::balance::algo {

IAlgorithmPtr Random();

}  // namespace commute::rpc::balance::algo
