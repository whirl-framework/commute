#include <commute/rpc/balance/load/const.hpp>

namespace commute::rpc::balance::load {

//////////////////////////////////////////////////////////////////////

class ConstNode final : public INode {
 public:
  ConstNode(IChannelPtr channel, LoadValue load)
      : channel_(std::move(channel)), load_(load) {
    //
  }

  LoadValue Load() override {
    return load_;
  }

  // IChannel

  BoxedFuture<ByteMessage> Call(Request request) override {
    return channel_->Call(std::move(request));
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    return channel_->Close();
  }

 private:
  IChannelPtr channel_;
  const LoadValue load_;
};

//////////////////////////////////////////////////////////////////////

class ConstMetric final : public ILoadMetric {
 public:
  ConstMetric(LoadValue value)
    : value_(value) {
  }

  INodePtr Measure(IChannelPtr node) override {
    return std::make_shared<ConstNode>(std::move(node), value_);
  }

 private:
  const LoadValue value_;
};

//////////////////////////////////////////////////////////////////////

ILoadMetricPtr Const(LoadValue load) {
  return std::make_shared<ConstMetric>(load);
}

}  // namespace commute::rpc::balance::load
