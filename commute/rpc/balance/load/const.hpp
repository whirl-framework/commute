#pragma once

#include <commute/rpc/balance/metric.hpp>

namespace commute::rpc::balance::load {

// Node with constant predefined load value
// For testing purposes

ILoadMetricPtr Const(LoadValue value);

}  // namespace commute::rpc::balance::load
