#include <commute/rpc/balance/load/ema.hpp>

#include <commute/rt/locate/clock.hpp>

#include <await/futures/combine/seq/handle.hpp>

#include <twist/ed/spin/lock.hpp>

#include <iostream>
#include <fmt/core.h>

namespace commute::rpc::balance::load {

//////////////////////////////////////////////////////////////////////

// Floating-Point Determinism
// https://randomascii.wordpress.com/2013/07/16/floating-point-determinism/

//////////////////////////////////////////////////////////////////////

class EmaCalculator {
 public:
  EmaCalculator(EmaParams params, ticktock::IWallClock& clock)
    : params_(params),
      clock_(clock),
      last_time_(clock_.Now()),
      value_(params.init_value) {
  }

  double Update(uint64_t new_value) {
    auto now = clock_.Now();
    // NB: consistent units for delta & window
    WindowUnits dt = now - last_time_;

    double w = 1.0 - exp(- (1.0 * dt.count()) / params_.window.count());

    value_ = new_value * w + (1.0 - w) * value_;
    last_time_ = now;

    return value_;
  }

 private:
  const EmaParams params_;
  ticktock::IWallClock& clock_;

  // State
  ticktock::WallTime last_time_;
  double value_;
};

//////////////////////////////////////////////////////////////////////

class LossyConcurrentEmaCalculator {
 public:
  LossyConcurrentEmaCalculator(EmaParams params)
    : ewma_(params, rt::WallClock()) {
  }

  void Update(uint64_t new_value) {
    if (spinlock_.TryLock()) {
      cached_value_.store(ewma_.Update(new_value));
      spinlock_.Unlock();
    } else {
      // Ignore concurrent updates
    }
  }

  double Get() {
    return cached_value_.load();
  }

 private:
  twist::ed::stdlike::atomic<double> cached_value_;
  twist::ed::SpinLock spinlock_;
  EmaCalculator ewma_;  // Guarded by spinlock
};

//////////////////////////////////////////////////////////////////////

class EmaRttNode final :
    public INode,
    public std::enable_shared_from_this<EmaRttNode> {
 public:
  EmaRttNode(IChannelPtr channel, EmaParams params)
      : channel_(std::move(channel)),
        clock_(rt::WallClock()),
        ewma_(params) {
    //
  }

  LoadValue Load() override {
    return static_cast<LoadValue>(ewma_.Get());
  }

  // IChannel

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto start_time = clock_.Now();

    return channel_->Call(std::move(request)) |
      await::futures::OnComplete([this, self = shared_from_this(), start_time](const fallible::Result<ByteMessage>&) mutable {
        std::chrono::nanoseconds latency = clock_.Now() - start_time;
        ewma_.Update(latency.count());
      });
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    return channel_->Close();
  }

 private:
  IChannelPtr channel_;
  ticktock::IWallClock& clock_;
  LossyConcurrentEmaCalculator ewma_;
};

//////////////////////////////////////////////////////////////////////

class EmaRttMetric final : public ILoadMetric {
 public:
  EmaRttMetric(EmaParams params)
      : params_(params) {
  }

  INodePtr Measure(IChannelPtr node) override {
    return std::make_shared<EmaRttNode>(std::move(node), params_);
  }

 private:
  const EmaParams params_;
};

//////////////////////////////////////////////////////////////////////

ILoadMetricPtr EmaRtt(EmaParams params) {
  return std::make_shared<EmaRttMetric>(params);
}

}  // namespace commute::rpc::balance::load
