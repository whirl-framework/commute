#include <commute/rpc/balance/load/inflight.hpp>

#include <await/futures/combine/seq/anyway.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace commute::rpc::balance::load {

//////////////////////////////////////////////////////////////////////

class InFlightNode final :
    public INode,
    public std::enable_shared_from_this<InFlightNode> {
 public:
  InFlightNode(IChannelPtr channel)
    : channel_(std::move(channel)) {
    //
  }

  LoadValue Load() override {
    return in_flight_.load();
  }

  // IChannel

  BoxedFuture<ByteMessage> Call(Request request) override {
    in_flight_.fetch_add(1);
    return channel_->Call(std::move(request)) |
      await::futures::Anyway([self = shared_from_this()]() {
        self->in_flight_.fetch_sub(1);
      });
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    return channel_->Close();
  }

 private:
  twist::ed::stdlike::atomic<LoadValue> in_flight_{0};
  IChannelPtr channel_;
};

//////////////////////////////////////////////////////////////////////

struct InFlightMetric final : ILoadMetric {
  virtual INodePtr Measure(IChannelPtr node) {
    return std::make_shared<InFlightNode>(std::move(node));
  }
};

//////////////////////////////////////////////////////////////////////

ILoadMetricPtr InFlight() {
  return std::make_shared<InFlightMetric>();
}

}  // namespace commute::rpc::balance::load
