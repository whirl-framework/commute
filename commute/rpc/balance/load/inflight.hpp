#pragma once

#include <commute/rpc/balance/metric.hpp>

namespace commute::rpc::balance::load {

// Number of in-flight requests

ILoadMetricPtr InFlight();

}  // namespace commute::rpc::balance::load
