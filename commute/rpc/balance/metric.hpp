#pragma once

#include <commute/rpc/balance/node.hpp>

namespace commute::rpc::balance {

struct ILoadMetric {
  virtual ~ILoadMetric() = default;

  virtual INodePtr Measure(IChannelPtr) = 0;
};

using ILoadMetricPtr = std::shared_ptr<ILoadMetric>;

}  // namespace commute::rpc::balance
