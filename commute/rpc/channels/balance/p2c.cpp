#include <commute/rpc/channels/balance/p2c.hpp>

#include <commute/rpc/balance/channel.hpp>
#include <commute/rpc/balance/algo/p2c.hpp>
#include <commute/rpc/balance/load/inflight.hpp>

namespace commute::rpc::channels {

IChannelPtr PowerOf2Choices(
    IResolverPtr resolver,
    IClientPtr client,
    balance::ILoadMetricPtr load) {
  return balance::Balancer(
      std::move(resolver),
      std::move(client),
      std::move(load),
      balance::algo::PowerOf2Choices());
}

IChannelPtr PowerOf2Choices(IResolverPtr resolver, IClientPtr client) {
  return PowerOf2Choices(
      std::move(resolver),
      std::move(client),
      balance::load::InFlight());
}

}  // namespace commute::rpc::channels
