#pragma once

#include <commute/rpc/core/client/channel.hpp>
#include <commute/rpc/core/client/client.hpp>

#include <commute/rpc/balance/metric.hpp>

#include <commute/rpc/resolve/static.hpp>
#include <commute/rpc/resolve/dynamic.hpp>

namespace commute::rpc::channels {

// P2C distributor + customizable load metric

IChannelPtr PowerOf2Choices(
    IResolverPtr resolver,
    IClientPtr client,
    balance::ILoadMetricPtr load);

// P2C distributor + InFlight load metric

IChannelPtr PowerOf2Choices(IResolverPtr resolver, IClientPtr client);

}  // namespace commute::rpc::channels
