#include <commute/rpc/channels/balance/rr.hpp>

#include <commute/rpc/balance/channel.hpp>
#include <commute/rpc/balance/algo/rr.hpp>
#include <commute/rpc/balance/load/const.hpp>

namespace commute::rpc::channels {

IChannelPtr RoundRobin(IResolverPtr resolver, IClientPtr client) {
  return balance::Balancer(
      std::move(resolver),
      std::move(client),
      balance::load::Const(0),
      balance::algo::RoundRobin());
}

}  // namespace commute::rpc::channels
