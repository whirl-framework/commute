#pragma once

#include <commute/rpc/core/client/channel.hpp>
#include <commute/rpc/core/client/client.hpp>

#include <commute/rpc/resolve/static.hpp>
#include <commute/rpc/resolve/dynamic.hpp>

namespace commute::rpc::channels {

IChannelPtr RoundRobin(IResolverPtr resolver, IClientPtr client);

}  // namespace commute::rpc::channels
