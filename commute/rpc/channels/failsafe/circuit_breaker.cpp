#include <commute/rpc/channels/failsafe/circuit_breaker.hpp>

#include <commute/rt/locate/executor.hpp>
#include <commute/rt/locate/timers.hpp>

#include <await/tasks/exe/strand.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <await/futures/make/after.hpp>
#include <await/futures/make/submit.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/handle.hpp>
#include <await/futures/run/go.hpp>

#include <fallible/error/codes.hpp>
#include <wheels/core/panic.hpp>

#include <timber/log/log.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

CircuitBreakerParams::CircuitBreakerParams()
  : max_failures(5),
    reset_timeout(std::chrono::seconds(10)),
    success_threshold(1) {
}

//////////////////////////////////////////////////////////////////////

class CircuitBreakerChannel final
    : public IChannel,
      public std::enable_shared_from_this<CircuitBreakerChannel> {

  enum class State {
    Closed,
    Open,
    HalfOpen
  };

 public:
  CircuitBreakerChannel(IChannelPtr channel,
                        CircuitBreakerParams params)
    : continue_(std::move(channel)),
      params_(params),
      strand_(rt::Executor()),
      logger_("Circuit-Breaker") {
    logger_.AddExtra({"peer", continue_->Peer()});
    SwitchToClosedState();
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    return await::futures::Submit(
        strand_,
        [self = shared_from_this(), request = std::move(request)]() mutable {
          return self->CallSeq(std::move(request));
        }
    ) | await::futures::Flatten();
  }

  live::Status Status() override {
    return status_.load();
  }

  const std::string& Peer() const override {
    return continue_->Peer();
  }

  void Close() override {
    continue_->Close();
  }

 private:
  // Circuit breaker

  BoxedFuture<ByteMessage> CallSeq(Request request) {
    switch (state_) {
      case State::Closed:
        return DoCall(std::move(request));
      case State::Open:
        TIMBER_LOG_DEBUG("Fail-fast: Unavailable");
        return Unavailable();
      case State::HalfOpen:
        if (probes_ > 0) {
          return DoProbeCall(std::move(request));
        } else {
          TIMBER_LOG_DEBUG("Fail-fast: Unavailable");
          return Unavailable();
        }
    }
  }

  void Update(size_t epoch, const fallible::Result<ByteMessage>& result) {
    if (epoch < Epoch()) {
      return;  // Ignore old responses
    }

    if (state_ == State::Closed) {
      if (result.IsOk()) {
        if (failures_ > 0) {
          TIMBER_LOG_DEBUG("Reset failure count");
        }
        failures_ = 0;
      } else {
        ++failures_;
        TIMBER_LOG_DEBUG("Increment failure count: {}", failures_);

        if (failures_ >= params_.max_failures) {
          SwitchToOpenState("Too many failures");
        }
      }
    } else if (state_ == State::Open) {
      // Impossible
      WHEELS_PANIC("Broken circuit breaker");
    } else if (state_ == State::HalfOpen) {
      TIMBER_LOG_DEBUG("Probe request completed");
      if (result.IsOk()) {
        ++oks_;
        if (oks_ == params_.success_threshold) {
          SwitchToClosedState();
        }
      } else {
        SwitchToOpenState("Probe request failed");
      }
    }
  }

  void SwitchToOpenState(std::string reason) {
    TIMBER_LOG_DEBUG("Switch to Open state: {}", reason);

    state_ = State::Open;
    status_.store(live::Status::Suspicious);
    ++switches_;

    await::futures::After(rt::Timers().Delay(params_.reset_timeout)) |
      await::futures::Via(strand_) |
      await::futures::Apply([self = shared_from_this()](wheels::Unit) {
        self->SwitchToHalfOpen();
      }) |
      await::futures::Detach();
  }

  void SwitchToHalfOpen() {
    TIMBER_LOG_DEBUG("Switch to Half-Open state after reset timeout");

    state_ = State::HalfOpen;
    status_.store(live::Status::Alive);
    ++switches_;
    probes_ = params_.success_threshold;
    oks_ = 0;
  }

  void SwitchToClosedState() {
    TIMBER_LOG_DEBUG("Switch to Closed state");

    state_ = State::Closed;
    status_.store(live::Status::Alive);
    ++switches_;
    failures_ = 0;
  }

  size_t Epoch() const {
    return switches_;
  }

 private:
  BoxedFuture<ByteMessage> Unavailable() {
    return futures::Fail<ByteMessage>(
        fallible::errors::Unavailable()
          .Domain("commute.rpc")
          .Reason("Circuit breaker is open")
          .Done());
  }

  BoxedFuture<ByteMessage> DoCall(Request request) {
    return continue_->Call(std::move(request)) |
      await::futures::Via(strand_) |
      await::futures::Apply([epoch = Epoch(), self = shared_from_this()](fallible::Result<ByteMessage> result) {
        self->Update(epoch, result);
        return result;
      });
  }

  BoxedFuture<ByteMessage> DoProbeCall(Request request) {
    TIMBER_LOG_DEBUG("Make probe call {}.{}", request.method.service, request.method.name);

    // Probe call
    auto call = DoCall(std::move(request));
    --probes_;

    // Restore probe
    return std::move(call) |
      await::futures::Via(strand_) |
      await::futures::OnCancel([epoch = Epoch(), this, self = shared_from_this()]() {
        ++probes_;  // Try again
      }) |
      await::futures::Via(rt::Executor());
  }

 private:
  IChannelPtr continue_;
  const CircuitBreakerParams params_;

  await::tasks::Strand strand_;

  State state_ = State::Closed;
  size_t failures_;  // Closed
  size_t probes_;  // Half-open
  size_t oks_;  // Half-open

  size_t switches_ = 0;

  twist::ed::stdlike::atomic<live::Status> status_{live::Status::Alive};

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr CircuitBreaker(IChannelPtr channel,
                           CircuitBreakerParams params) {
  return std::make_shared<CircuitBreakerChannel>(
      std::move(channel),
      params);
}

}  // namespace commute::rpc::channels
