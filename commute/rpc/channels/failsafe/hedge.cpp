#include <commute/rpc/channels/failsafe/hedge.hpp>

#include <commute/rt/locate/timers.hpp>

#include <commute/concurrency/futures/types/naked.hpp>

#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/flat_map.hpp>
#include <await/futures/syntax/or.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <await/futures/make/after.hpp>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

class HedgeChannel final : public IChannel {
 public:
  HedgeChannel(IChannelPtr channel, HedgeParams params)
    : channel_(std::move(channel)),
      params_(params) {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto primary = channel_->Call(request);

    auto backup = await::futures::After(Delay())
                    >> channel_->Call(std::move(request));

    return std::move(primary) or std::move(backup);
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    channel_->Close();
  }

 private:
  await::timers::Delay Delay() const {
    // TODO: Estimate request latency
    return rt::Timers().Delay(params_.delay);
  }

 private:
  IChannelPtr channel_;
  const HedgeParams params_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Hedge(IChannelPtr channel, HedgeParams params) {
  return std::make_shared<HedgeChannel>(
      std::move(channel),
      params);
}

}  // namespace commute::rpc::channels
