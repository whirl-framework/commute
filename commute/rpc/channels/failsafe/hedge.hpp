#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <chrono>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

struct HedgeParams {
  std::chrono::milliseconds delay;

  HedgeParams& Delay(std::chrono::milliseconds millis) {
    delay = millis;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Hedge(IChannelPtr channel, HedgeParams params);

}  // namespace commute::rpc::channels
