#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <commute/rpc/retry/backoff.hpp>
#include <commute/rpc/retry/budget.hpp>

#include <commute/rpc/retry/budget/unlimited.hpp>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

IChannelPtr Retry(IChannelPtr channel,
                  retry::IBackoffPtr backoff,
                  retry::IBudgetPtr budget = retry::budget::Unlimited());

}  // namespace commute::rpc::channels
