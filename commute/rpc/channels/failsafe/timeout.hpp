#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <chrono>
#include <optional>

namespace commute::rpc {

namespace channels {

//////////////////////////////////////////////////////////////////////

// https://vorpus.org/blog/timeouts-and-cancellation-for-humans/

//////////////////////////////////////////////////////////////////////

struct TimeoutParams {
  std::optional<std::chrono::milliseconds> timeout;

  TimeoutParams() = default;

  TimeoutParams& Value(std::chrono::milliseconds millis) {
    timeout = millis;
    return *this;
  }
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Timeout(IChannelPtr channel,
                    TimeoutParams params = TimeoutParams());

}  // namespace channels

//////////////////////////////////////////////////////////////////////

namespace headers::keys {

std::string_view DeadLine();

}  // namespace headers::keys

}  // namespace commute::rpc
