#include <commute/rpc/channels/observe/log.hpp>

#include <await/futures/combine/seq/apply.hpp>

#include <timber/log/log.hpp>

namespace commute::rpc::channels {

//////////////////////////////////////////////////////////////////////

class LoggingChannel :
    public IChannel,
    public std::enable_shared_from_this<LoggingChannel> {
 public:
  LoggingChannel(IChannelPtr channel)
      : continue_(std::move(channel)),
        logger_("Log-Channel") {
    logger_.AddExtra({"peer", continue_->Peer()});
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    TIMBER_LOG_INFO("Call {}", request.method);

    return continue_->Call(request) | await::futures::Apply([this, self = shared_from_this(), method = request.method](fallible::Result<ByteMessage> result) {
      if (result.IsOk()) {
        TIMBER_LOG_INFO("Call {} completed", method);
      } else {
        TIMBER_LOG_WARN("Call {} failed: {}", method, result.Error().Describe());
      }
      return result;
    });
  }

  live::Status Status() override {
    return live::Status::Alive;
  }

  const std::string& Peer() const override {
    return continue_->Peer();
  }

  void Close() override {
    continue_->Close();
  }

 private:
  IChannelPtr continue_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Log(IChannelPtr channel) {
  return std::make_shared<LoggingChannel>(std::move(channel));
}

}  // namespace commute::rpc::channels
