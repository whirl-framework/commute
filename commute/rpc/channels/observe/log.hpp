#pragma once

#include <commute/rpc/core/client/channel.hpp>

namespace commute::rpc::channels {

IChannelPtr Log(IChannelPtr channel);

}  // namespace commute::rpc::channels
