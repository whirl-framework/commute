#pragma once

#include <commute/rpc/core/client/call.hpp>

/*
 * Unary RPC:
 *
 * // Prepare request message
 * proto::echo::Request req;
 * req.set_data("Hello");
 *
 * auto future = commute::rpc::Call("EchoService.Echo")
 *           .Request(req)
 *           .Via(channel)
 *           .With(context)                                        // optional
 *           .AtLeastOnce() or .AtMostOnce() or .AttemptsLimit(3)  // optional
 *           .Timeout(1s)                                          // optional
 *           .DeepCancel(true)                                     // optional
 *           .Done()
 *           .As<proto::echo::Response>();
 *
 * .Future().As<Response>() is optional:
 *
 * commute BoxedBoxedFuture<proto::echo::Response> f =
 *    commute::rpc::Call("EchoService.Echo").Request(req).Via(channel);
 */
