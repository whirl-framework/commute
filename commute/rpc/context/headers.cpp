#include <commute/rpc/context/headers.hpp>

#include <fmt/core.h>

namespace commute::rpc::headers {

//////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& out, const Value& value) {
  if (value.IsString()) {
    out << value.AsString();
  } else if (value.IsUInt64()) {
    out << value.AsUInt64();
  } else {
    assert(value.IsBool());
    out << (value.AsBool() ? "true" : "false") << std::endl;
  }
  return out;
}

//////////////////////////////////////////////////////////////////////

static constexpr std::string_view kHeaderKeyPrefix = "commute.rpc.header.";

//////////////////////////////////////////////////////////////////////

carry::Key ContextKey(std::string_view name) {
  return fmt::format("{}{}", kHeaderKeyPrefix, name);
}

std::string NameFrom(const carry::Key& key) {
  return key.substr(kHeaderKeyPrefix.length(), key.length());
}

//////////////////////////////////////////////////////////////////////

std::vector<Header> List(const carry::Context& context) {
  std::vector<Header> headers;

  auto keys = context.CollectKeys(kHeaderKeyPrefix);

  for (const auto& key : keys) {
    headers.push_back({NameFrom(key), context.Get<Value>(key)});
  }

  return headers;
}

}  // namespace commute::rpc::headers
