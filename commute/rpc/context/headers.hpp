#pragma once

#include <string>
#include <variant>
#include <vector>

#include <carry/context.hpp>

#include <ostream>

namespace commute::rpc::headers {

//////////////////////////////////////////////////////////////////////

/*
 * Value
 *
 * Supported types:
 *    string
 *    uint64
 *    bool
 */

class Value {
 public:
  // Constructors

  Value(std::string str)
      : storage_(std::move(str)) {
  }

  Value(const char* str)
      : storage_(std::string(str)) {
  }

  Value(uint64_t value)
      : storage_(value) {
  }

  Value(bool flag)
      : storage_(flag) {
  }

  // Named constructors

  static Value String(std::string value) {
    return Value(std::move(value));
  }

  static Value UInt64(uint64_t value) {
    return Value(value);
  }

  static Value Bool(bool value) {
    return Value(value);
  }

  // Is

  bool IsString() const {
    return storage_.index() == 0;
  }

  bool IsUInt64() const {
    return storage_.index() == 1;
  }

  bool IsBool() const {
    return storage_.index() == 2;
  }

  // As

  std::string AsString() const {
    return std::get<0>(storage_);
  }

  uint64_t AsUInt64() const {
    return std::get<1>(storage_);
  }

  bool AsBool() const {
    return std::get<2>(storage_);
  }

 private:
  std::variant<std::string, uint64_t, bool> storage_;
};

//////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream& out, const Value& value);

//////////////////////////////////////////////////////////////////////

carry::Key ContextKey(std::string_view name);
std::string NameFrom(const carry::Key& key);

//////////////////////////////////////////////////////////////////////

struct Header {
  std::string name;
  Value value;
};

//////////////////////////////////////////////////////////////////////

// Get

inline std::optional<Value> TryGet(const carry::Context& context, std::string_view name) {
  return context.TryGet<Value>(ContextKey(name));
}

inline Value GetOr(const carry::Context& context, std::string_view name, Value or_value) {
  return context.GetOr(ContextKey(name), or_value);
}

// List

std::vector<Header> List(const carry::Context& context);

}  // namespace commute::rpc::headers
