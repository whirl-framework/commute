#include <commute/rpc/core/client/call.hpp>

#include <commute/rpc/context/headers.hpp>

#include <await/tasks/curr/context.hpp>

namespace commute::rpc {

namespace detail {

ContextBuilder::ContextBuilder()
  : wrapper_(await::tasks::curr::Context()) {
}

void ContextBuilder::Reset(carry::Context base) {
  wrapper_.ReWrap(std::move(base));
}

void ContextBuilder::LimitAttempts(size_t limit) {
  wrapper_.Set<uint64_t>(params::ContextKey("attempts"), limit);
}

// Cancellation

void ContextBuilder::WithTimeout(std::chrono::milliseconds timeout) {
  wrapper_.Set(params::ContextKey("timeout.ms"), timeout);
}

void ContextBuilder::DeepCancel(bool enable) {
  wrapper_.Set(headers::ContextKey("deep.cancel"), headers::Value(enable));
}

}  // namespace detail

}  // namespace commute::rpc
