#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <commute/rpc/core/request/message.hpp>
#include <commute/rpc/core/request/method.hpp>
#include <commute/rpc/errors/domain.hpp>

#include <commute/rpc/context/headers.hpp>
#include <commute/rpc/context/params.hpp>

#include <commute/rpc/formats/detector.hpp>

#include <carry/wrap.hpp>

#include <pickle/serialize.hpp>

#include <optional>
#include <chrono>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

namespace detail {

// Pipeline:
// --Call--> RequestCaller
// --Request--> ViaCaller
// --Via--> Caller
// --Start--> Format::Future

//////////////////////////////////////////////////////////////////////

class ContextBuilder {
 public:
  ContextBuilder();

  void Reset(carry::Context base);

  void LimitAttempts(size_t limit);
  void WithTimeout(std::chrono::milliseconds timeout);
  void DeepCancel(bool enable);

  template <typename TValue>
  void SetParam(std::string_view name, TValue value) {
    wrapper_.Set(params::ContextKey(name), value);
  }

  void SetHeader(std::string_view name, headers::Value value) {
    wrapper_.Set(headers::ContextKey(name), std::move(value));
  }

  carry::Context Done() {
    return wrapper_.Done();
  }

 private:
  carry::ReWrapper wrapper_;
};

//////////////////////////////////////////////////////////////////////

template <typename Format>
class [[nodiscard]] Caller {
 public:
  Caller(Method method, ByteMessage request, IChannelPtr channel)
      : method_(method),
        request_(std::move(request)),
        channel_(std::move(channel)) {
  }

  // Context

  Caller& With(carry::Context context);

  // Retries

  Caller& AtMostOnce() {
    return AttemptsLimit(1);
  }

  Caller& AtLeastOnce() {
    return AttemptsLimit(0);
  }

  Caller& AttemptsLimit(size_t count) {
    context_.LimitAttempts(count);
    return *this;
  }

  // Cancellation

  Caller& Timeout(std::chrono::milliseconds millis) {
    context_.WithTimeout(millis);
    return *this;
  }

  Caller& DeepCancel(bool enable) {
    context_.DeepCancel(enable);
    return *this;
  }

  // Params

  template <typename T>
  Caller& Param(std::string_view name, T value) {
    context_.SetParam(name, std::move(value));
    return *this;
  }

  // Headers

  Caller& Header(std::string_view name, headers::Value value) {
    context_.SetHeader(name, std::move(value));
    return *this;
  }

  // Start

  auto Done() {
    return Format::FromFutureBytes(Call());
  }

 private:
  BoxedFuture<ByteMessage> Call() {
    return channel_->Call({std::move(method_),
                           std::move(request_),
                           context_.Done()});
  }

 private:
  Method method_;
  ByteMessage request_;
  IChannelPtr channel_;
  ContextBuilder context_;
};

//////////////////////////////////////////////////////////////////////

template <typename Format>
class [[nodiscard]] ViaCaller {
 public:
  ViaCaller(Method method, ByteMessage request)
      : method_(method), request_(std::move(request)) {
  }

  auto Via(IChannelPtr channel)&& {
    return Caller<Format>(std::move(method_),
                          std::move(request_),
                          std::move(channel));
  }

 private:
  Method method_;
  ByteMessage request_;
};

//////////////////////////////////////////////////////////////////////

class [[nodiscard]] RequestCaller {
 public:
  RequestCaller(Method method) : method_(method) {
  }

  template <typename TRequest>
  auto Request(const TRequest& request) {
    using Format = typename formats::FormatDetector<TRequest>::Format;

    auto request_bytes = Format::ToBytes(request);

    return ViaCaller<Format>{method_, request_bytes};
  }

 private:
  Method method_;
};

}  // namespace detail

//////////////////////////////////////////////////////////////////////

inline detail::RequestCaller Call(const std::string& method_str) {
  auto method = Method::Parse(method_str);
  return detail::RequestCaller{method};
}

}  // namespace commute::rpc
