#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

#include <commute/rpc/live/failure_detector.hpp>

#include <memory>

namespace commute::rpc {

// Represents remote server or set of replicas

struct IChannel : IEndpoint, live::IFailureDetector {
  // Unary Call(Request)

  // Peer address or description
  virtual const std::string& Peer() const = 0;

  // IEndpoint
  const std::string& Name() const override {
    return Peer();
  }
  virtual void Close() = 0;
};

using IChannelPtr = std::shared_ptr<IChannel>;

}  // namespace commute::rpc
