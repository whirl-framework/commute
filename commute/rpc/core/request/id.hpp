#pragma once

#include <snowflake/id/id.hpp>

namespace commute::rpc {

using RequestId = snowflake::id::Id;

}  // namespace commute::rpc
