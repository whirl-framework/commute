#pragma once

#include <commute/rpc/core/request/request.hpp>

#include <commute/concurrency/futures/types/boxed.hpp>

#include <memory>

namespace commute::rpc {

struct IEndpoint {
  virtual ~IEndpoint() = default;

  // Unary
  virtual BoxedFuture<ByteMessage> Call(Request request) = 0;

  virtual const std::string& Name() const = 0;
};

using IEndpointPtr = std::shared_ptr<IEndpoint>;

}  // namespace commute::rpc
