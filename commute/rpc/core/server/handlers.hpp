#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

struct IHandlerManager : IEndpoint {
  virtual void CancelAll() = 0;
};

using IHandlerManagerPtr = std::shared_ptr<IHandlerManager>;

//////////////////////////////////////////////////////////////////////

IHandlerManagerPtr ManageHandlers(IEndpointPtr endpoint);

}  // namespace commute::rpc
