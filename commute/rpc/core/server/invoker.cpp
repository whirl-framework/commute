#include <commute/rpc/core/server/invoker.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/rt/locate/executor.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <await/futures/make/just.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/flatten.hpp>

#include <timber/log/log.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

class Invoker : public IEndpoint {
 public:
  Invoker(IServicePtr service)
      : service_(std::move(service)) {
    service_->Initialize();
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    if (request.method.service != Name()) {
      return futures::Fail<ByteMessage>(UnexpectedService(request.method));
    }

    if (!service_->Has(request.method.name)) {
      return futures::Fail<ByteMessage>(MethodNotFound(request.method));
    }

    return await::futures::Just()
      | await::futures::Via(rt::Executor())
      | await::futures::With(std::move(request.context))
      | await::futures::Apply([service = service_, request](wheels::Unit) {
          return service->Invoke(request.method.name, request.body);
        })
      | await::futures::Flatten();
  }

  const std::string& Name() const override {
    return service_->Name();
  }

 private:
  fallible::Error UnexpectedService(const Method& method) {
    return fallible::errors::NotFound()
        .Domain(rpc::errors::Domain())
        .Reason(fmt::format("Unexpected service: {}", method.service))
        .Done();
  }

  fallible::Error MethodNotFound(const Method& method) {
    return fallible::errors::NotFound()
      .Domain(rpc::errors::Domain())
      .Reason(fmt::format(
          "Requested method `{}` not found in service `{}`",
          method.name, method.service))
      .Done();
  }

 private:
  IServicePtr service_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr Invoke(IServicePtr service) {
  return std::make_shared<Invoker>(
      std::move(service));
}

}  // namespace commute::rpc
