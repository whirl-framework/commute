#pragma once

#include <commute/rpc/core/server/endpoint.hpp>
#include <commute/rpc/core/service/service.hpp>

namespace commute::rpc {

IEndpointPtr Invoke(IServicePtr service);

}  // namespace commute::rpc
