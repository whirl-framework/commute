#pragma once

#include <commute/rpc/core/request/method.hpp>
#include <commute/rpc/core/request/id.hpp>

#include <carry/context.hpp>

#include <string>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

struct Metadata {
  std::string peer;
  RequestId id;
  Method method;
};

//////////////////////////////////////////////////////////////////////

const carry::Key& MetadataKey();

Metadata GetMetadata(const carry::Context& context);

}  // namespace commute::rpc
