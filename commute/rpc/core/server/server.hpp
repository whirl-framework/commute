#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

namespace commute::rpc {

struct IServer {
  virtual ~IServer() = default;

  // One-shot
  virtual void Serve(IEndpointPtr endpoint) = 0;
  virtual void Shutdown() = 0;

  virtual std::string Port() const = 0;
};

using IServerPtr = std::shared_ptr<IServer>;

}  // namespace commute::rpc
