#pragma once

#include <wheels/core/preprocessor.hpp>

// In service template context

#define COMMUTE_RPC_REGISTER_METHOD(method) \
  RegisterMethod(TO_STRING(method), &ThisService::method)
