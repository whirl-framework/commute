#include <commute/rpc/filters/deadline.hpp>

#include <commute/rt/locate/timers.hpp>

#include <commute/rpc/errors/domain.hpp>
#include <commute/rpc/context/headers.hpp>

// headers::key::DeadLine
#include <commute/rpc/channels/failsafe/timeout.hpp>

#include <await/futures/make/after.hpp>
#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/timeout.hpp>

#include <ticktock/clocks/time_units.hpp>

#include <timber/log/log.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class DeadlineFilter final : public IEndpoint {
 public:
  DeadlineFilter(IEndpointPtr endpoint)
      : continue_(std::move(endpoint)),
        logger_("RPC-DeadLine-Filter") {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto deadline = headers::TryGet(request.context, headers::keys::DeadLine());

    // TODO: check deadline before execution

    auto call = continue_->Call(std::move(request));

    if (deadline && (deadline->AsUInt64() > 0)) {
      return WithTimeout(std::move(call), ToTimeout(deadline->AsUInt64()));
    }

    return call;
  }

  const std::string& Name() const override {
    return continue_->Name();
  }

 private:
  static std::chrono::milliseconds ToTimeout(int64_t deadline) {
    ticktock::TimeUnits since_epoch(deadline);
    return std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
  }

  BoxedFuture<ByteMessage> WithTimeout(
      BoxedFuture<ByteMessage> call,
      std::chrono::milliseconds timeout) {

    return std::move(call)
            | await::futures::WithTimeout(
                rt::Timers().Delay(timeout));
  }

  static fallible::Error TimedOut() {
    return fallible::errors::TimedOut()
        .Domain(rpc::errors::Domain())
        .Reason("Request timed out")
        .Done();
  }

 private:
  IEndpointPtr continue_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr DeadLine(IEndpointPtr endpoint) {
  return std::make_shared<DeadlineFilter>(
      std::move(endpoint));
}

}  // namespace commute::rpc::filters
