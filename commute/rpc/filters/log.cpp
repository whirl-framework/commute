#include <commute/rpc/filters/trace.hpp>

#include <commute/rpc/core/server/metadata.hpp>

#include <await/futures/combine/seq/apply.hpp>

#include <timber/log/log.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class LoggingFilter final
    : public IEndpoint,
      public std::enable_shared_from_this<LoggingFilter> {

 public:
  LoggingFilter(IEndpointPtr endpoint)
      : continue_(std::move(endpoint)),
        logger_("RPC-Server") {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto meta = GetMetadata(request.context);

    TIMBER_LOG_INFO("Handle request {} from peer {} (request-id = {})", meta.method, meta.peer, meta.id);

    return continue_->Call(std::move(request)) |
        await::futures::Apply([this, self = shared_from_this(), meta](fallible::Result<ByteMessage> result) {
          if (result.IsOk()) {
            TIMBER_LOG_INFO("Request {} (request-id = {}) completed", meta.method, meta.id);
          } else {
            TIMBER_LOG_INFO("Request {} (request-id = {}) failed: ", meta.method, meta.id, result.Error().Describe());
          }
          return result;  // Forward
        });
  }

  const std::string& Name() const override {
    return continue_->Name();
  }

 private:
  IEndpointPtr continue_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr Log(IEndpointPtr endpoint) {
  return std::make_shared<LoggingFilter>(std::move(endpoint));
}

}  // namespace commute::rpc::filters
