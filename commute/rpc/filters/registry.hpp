#pragma once

#include <commute/rpc/core/server/endpoint.hpp>

#include <map>
#include <memory>

namespace commute::rpc::filters {

class Registry {
 public:
  void Register(std::string name, IEndpointPtr service);

  bool Has(const std::string& name) const;

  IEndpointPtr Get(const std::string& name);
  IEndpointPtr TryGet(const std::string& name);

 private:
  // Names -> IEndpoint-s
  std::map<std::string, IEndpointPtr> entries_;
};

}  // namespace commute::rpc::filters
