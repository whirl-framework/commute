#include <commute/rpc/filters/semaphore.hpp>

#include <commute/concurrency/semaphore/async.hpp>
#include <commute/concurrency/futures/make/fail.hpp>

#include <commute/rpc/context/headers.hpp>

#include <await/futures/make/value.hpp>
#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/flat_map.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/with.hpp>

#include <carry/new.hpp>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class SemaphoreFilter final
    : public IEndpoint,
      public std::enable_shared_from_this<SemaphoreFilter> {
 public:
  SemaphoreFilter(IEndpointPtr endpoint,
              SemaphoreParams params)
    : continue_(std::move(endpoint)),
      permits_({params.permits, params.queue_limit}) {
  }

 private:
  // Permit granted
  auto Continue(Request request) {
    return continue_->Call(std::move(request))  |
      await::futures::Anyway([self = shared_from_this()]() {
        // Release permit after request completion / cancellation
        self->permits_.Release();
      });
  }

  static Future<ByteMessage> auto Reject(fallible::Error overflow) {
    auto ctx = carry::New().Set(headers::ContextKey("retriable"), headers::Value::Bool(true)).Done();
    return futures::Fail<ByteMessage>(overflow) | await::futures::With(std::move(ctx));
  }

 public:
  // IEndpoint

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto permit = permits_.Acquire();

    return std::move(permit) |
      await::futures::Apply([self = shared_from_this(), request = std::move(request)](fallible::Status permit) mutable -> BoxedFuture<ByteMessage> {
        if (permit.IsOk()) {
          return self->Continue(std::move(request));
        } else {
          return self->Reject(permit.Error());
        }
      }) | await::futures::Flatten();
  }

  const std::string& Name() const override {
    return continue_->Name();
  }

 private:
  IEndpointPtr continue_;
  concurrency::AsyncSemaphore permits_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr Semaphore(IEndpointPtr endpoint,
                       SemaphoreParams params) {
  return std::make_shared<SemaphoreFilter>(
      std::move(endpoint),
      params);
}

}  // namespace commute::rpc::filters
