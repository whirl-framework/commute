#include <commute/rpc/filters/trace.hpp>

#include <commute/rpc/context/headers.hpp>

#include <commute/rt/locate/tracer.hpp>

#include <await/futures/combine/seq/anyway.hpp>

#include <timber/trace/carry.hpp>

#include <timber/log/log.hpp>

#include <fmt/core.h>

namespace commute::rpc::filters {

//////////////////////////////////////////////////////////////////////

class TracingFilter final : public IEndpoint {
 public:
  TracingFilter(IEndpointPtr endpoint)
      : continue_(std::move(endpoint)),
        tracer_(rt::Tracer()),
        logger_("RPC-Tracer") {
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    auto span = StartSpan(request.method, request.context);

    // Rewrite trace context
    request.context = timber::trace::Wrap(std::move(request.context), span);

    return continue_->Call(std::move(request)) |
      await::futures::Anyway([span]() mutable {
          std::move(span).Finish();  // TODO: Finish or Cancel
      });
  }

  const std::string& Name() const override {
    return continue_->Name();
  }

 private:
  std::string SpanName(const Method& method) const {
    return fmt::format("Server.{}.{}", method.service, method.name);
  }

  // From headers
  std::optional<timber::trace::SpanContext> TryGetSpanContext(const carry::Context& context) {
    if (auto trace_id = headers::TryGet(context, "tracing.trace_id")) {
      auto span_id = headers::TryGet(context, "tracing.span_id");
      return timber::trace::SpanContext(trace_id->AsString(), span_id->AsString());
    } else {
      return std::nullopt;
    }
  }

  timber::trace::References RefsFrom(const carry::Context& context) {
    if (auto span_context = TryGetSpanContext(context)) {
      return timber::trace::References::ChildOf(*span_context);
    } else {
      return timber::trace::References::Empty();
    }
  }

  timber::trace::Span StartSpan(const Method& method,
                          const carry::Context& context) {
    return tracer_.StartSpan(SpanName(method), RefsFrom(context));
  }

 private:
  IEndpointPtr continue_;
  timber::trace::ITracer& tracer_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

IEndpointPtr Trace(IEndpointPtr endpoint) {
  return std::make_shared<TracingFilter>(std::move(endpoint));
}

}  // namespace commute::rpc::filters
