#pragma once

#include <commute/rpc/core/service/service.hpp>

namespace commute::rpc {

IServicePtr MakeHealthService();

}  // namespace commute::rpc
