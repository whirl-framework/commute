#include <commute/rpc/proto/jet/client/socket.hpp>

#include <commute/rpc/errors/domain.hpp>
#include <commute/rpc/context/headers.hpp>

#include <commute/rpc/proto/jet/wire/method.hpp>
#include <commute/rpc/proto/jet/wire/context.hpp>
#include <commute/rpc/proto/jet/wire/error.hpp>
#include <commute/rpc/proto/jet/wire/checksum.hpp>
#include <commute/rpc/proto/jet/wire/header.hpp>

#include <commute/rt/locate/executor.hpp>
#include <commute/rt/locate/ids.hpp>
#include <commute/rt/locate/transport.hpp>

#include <commute/concurrency/futures/make/fail.hpp>
#include <commute/concurrency/futures/types/naked.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>

#include <carry/empty.hpp>

#include <pickle/serialize.hpp>

#include <fallible/error/error.hpp>
#include <fallible/error/codes.hpp>
#include <fallible/result/make.hpp>

#include <timber/log/log.hpp>

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

namespace commute::rpc::jet {

SocketChannel::SocketChannel(std::string peer)
  : peer_address_(std::move(peer)),
    executor_(rt::Executor()),
    transport_(rt::Transport()),
    ids_(rt::Ids()),
    strand_(executor_),
    logger_("Socket-Channel") {
  logger_.AddExtra({"peer", peer_address_});
}

SocketChannel::~SocketChannel() {
  // NB: strand is empty (each task in strand holds strong reference to channel!
  // Close();
}

BoxedFuture<ByteMessage> SocketChannel::Call(Request request) {
  return await::futures::Submit(strand_, [self = shared_from_this(),
                                           request = std::move(request)]() mutable {
    return self->Send(std::move(request));
  }) |
    await::futures::Flatten() |
    await::futures::Via(executor_);
}

void SocketChannel::Close() {
  // Do we need strong ref?
  auto close = [self = shared_from_this()]() {
    self->DoClose();
  };
  // TODO: ???
  // await::futures::SyncVia(&strand_, std::move(close));
}

RequestId SocketChannel::GenerateRequestId() {
  return ids_.GenerateId();
}

BoxedFuture<ByteMessage> SocketChannel::Send(Request request) {
  TIMBER_LOG_INFO("Request {}.{}", peer_address_, request.method);

  transport::ISocketPtr& socket = GetTransportSocket();

  if (!socket->IsConnected()) {
    // Fail RPC immediately
    auto error = fallible::errors::Disconnected()
                      .Domain(rpc::errors::Domain())
                      .Reason(fmt::format(
                          "Transport socket to peer {} is disconnected", peer_address_))
                      .Done();

    return futures::Fail<ByteMessage>(std::move(error));
  }

  // Generate Id
  auto id = GenerateRequestId();

  // Send request message
  {
    ::proto::commute::rpc::jet::Request wire_request;

    // Prepare request message
    {
      wire_request.set_id(id);
      wire_request.set_peer(peer_address_);
      wire_request.mutable_method()->set_service(request.method.service);
      wire_request.mutable_method()->set_name(request.method.name);
      wire_request.set_body(request.body);

      // Propagated context
      wire::SetContext(wire_request, request.context);

      // Compute checksum
      wire_request.set_checksum(wire::ComputeChecksum(wire_request));
    }

    socket->Send(pickle::Serialize(wire_request));
  }

  // Contract
  auto [f, p] = await::futures::Contract<fallible::Result<ByteMessage>>();

  // Attach cancellation handler
  p.CancelSubscribe(strand_, [self = shared_from_this(), id]() {
    self->CancelRequest(id);
  });

  // Add entry to active requests map
  {
    ActiveRequest active;

    active.request = std::move(request);
    active.id = id;
    active.promise = std::move(p);

    requests_.emplace(id, std::move(active));
  }

  return std::move(f);
}

static futures::Output<ByteMessage> MakeOutput(ByteMessage body, carry::Context user) {
  await::futures::Context context{};
  context.user = user;
  return {fallible::Ok(body), std::move(context)};
}

void SocketChannel::HandleResponse(const transport::Message& message) {
  TIMBER_LOG_DEBUG("Handle response message from {}", peer_address_);

  ::proto::commute::rpc::jet::Response wire_response;
  pickle::Deserialize(message, &wire_response);

  if (wire::ComputeChecksum(wire_response) != wire_response.checksum()) {
    WHEELS_PANIC("Response checksum mismatch");  // TODO: Can we recover?
  }

  auto request_it = requests_.find(wire_response.request_id());

  if (request_it == requests_.end()) {
    TIMBER_LOG_WARN("Request with id {} not found", wire_response.request_id());
    return;  // Probably duplicated response message from transport layer?
  }

  ActiveRequest active = std::move(request_it->second);
  requests_.erase(request_it);

  auto user_context = wire::GetContext(wire_response);

  if (wire_response.error().code() == 0) {
    TIMBER_LOG_INFO("Request {}.{} (request-id = {}) completed", peer_address_, active.request.method,
                    wire_response.request_id());


    std::move(*active.promise).Set(MakeOutput(wire_response.body(), user_context));
  } else {
    Fail(active, wire::GetError(wire_response), user_context);
  }
}

// Best effort
void SocketChannel::SendCancelRequest(const RequestId& request_id, const Method& method) {
  if (!socket_ || !socket_->IsConnected()) {
    return;
  }

  ::proto::commute::rpc::jet::Request wire_request;

  // Prepare request message
  {
    wire_request.set_id(request_id);
    wire::SetMethod(wire_request.mutable_method(), method);

    {
      auto* header = wire_request.mutable_headers()->Add();
      wire::SetHeader(header, {"cancel", headers::Value::Bool(true)});
    }

    wire_request.set_checksum(wire::ComputeChecksum(wire_request));
  }

  TIMBER_LOG_INFO("Send cancellation request to {} for request with request-id = {}",
           socket_->Peer(), request_id);

  socket_->Send(pickle::Serialize(wire_request));
}

void SocketChannel::CancelRequest(const RequestId& id) {
  auto request_it = requests_.find(id);

  if (request_it == requests_.end()) {
    TIMBER_LOG_DEBUG("Failed to cancel request with id {}: request not found, probably already completed", id);
    return;
  }

  ActiveRequest& active = request_it->second;

  // Cancel client future
  std::move(*active.promise).Cancel();

  TIMBER_LOG_INFO("Request {}.{} (request-id = {}) cancelled on client side", peer_address_, active.request.method, id);

  if (auto deep_cancel = headers::TryGet(active.request.context, "deep.cancel")) {
    if (deep_cancel->AsBool()) {
      SendCancelRequest(id, active.request.method);
    }
  }

  // Erase request entry
  requests_.erase(request_it);
}

void SocketChannel::LostPeer() {
  auto requests = std::move(requests_);
  requests_.clear();

  TIMBER_LOG_WARN("Transport connection to peer {} lost, fail {} pending request(s)",
                  peer_address_, requests.size());

  // Next Call triggers reconnect
  socket_.reset();

  for (auto& [_, request] : requests) {
    auto error = fallible::Err(fallible::ErrorCodes::Disconnected)
                     .Domain(rpc::errors::Domain())
                     .Reason(fmt::format(
                         "Transport socket to peer {} is disconnected", peer_address_))
                     .Done();

    Fail(request, std::move(error), carry::Empty());
  }
}

void SocketChannel::DoClose() {
  TIMBER_LOG_INFO("Close transport socket");
  if (socket_ && socket_->IsConnected()) {
    socket_->Close();
  }
}

transport::ISocketPtr& SocketChannel::GetTransportSocket() {
  if (socket_ && socket_->IsConnected()) {
    return socket_;
  }
  TIMBER_LOG_DEBUG("Reconnect to peer {}", peer_address_);
  socket_ = transport_.ConnectTo(peer_address_, weak_from_this());
  return socket_;
}

static commute::futures::Output<ByteMessage> MakeFailure(fallible::Error error, carry::Context user) {
  await::futures::Context ctx{};
  ctx.user = user;
  return {std::move(fallible::Fail(error)), ctx};
}

void SocketChannel::Fail(ActiveRequest& active, fallible::Error error, carry::Context user) {
  TIMBER_LOG_WARN("Request {}.{} (request-id = {}) failed: {}", peer_address_, active.request.method,
                  active.id, error.Describe());
  std::move(*active.promise).Set(MakeFailure(error, user));
}

}  // namespace commute::rpc::jet
