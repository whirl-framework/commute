#include <commute/rpc/proto/jet/server/impl.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/rpc/health/health.hpp>

#include <commute/rpc/proto/jet/wire/response.hpp>
#include <commute/rpc/proto/jet/wire/checksum.hpp>
#include <commute/rpc/proto/jet/wire/context.hpp>
#include <commute/rpc/proto/jet/wire/error.hpp>
#include <commute/rpc/proto/jet/wire/method.hpp>

#include <commute/rt/locate/transport.hpp>
#include <commute/rt/locate/executor.hpp>

#include <pickle/serialize.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/apply.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/run/go.hpp>

#include <await/tasks/curr/context.hpp>

#include <fallible/error/codes.hpp>

#include <timber/log/log.hpp>

#include <wheels/core/panic.hpp>

#include <fmt/core.h>

namespace commute::rpc::jet {

Server::Server(std::string port)
    : port_(std::move(port)),
    executor_(rt::Executor()),
    transport_(rt::Transport()),
    logger_("RPC-Server") {
}

void Server::Serve(IEndpointPtr endpoint) {
  handlers_ = ManageHandlers(std::move(endpoint));
  endpoint_ = handlers_;

  server_ = transport_.Serve(port_, weak_from_this());
  TIMBER_LOG_INFO("Serving at port {}", port_);
}

void Server::Shutdown() {
  if (server_) {
    server_->Shutdown();
  }

  handlers_->CancelAll();
}

// ITransportHandler

void Server::HandleMessage(const transport::Message& message,
                           transport::ISocketPtr client) {

  // Process request

  commute::Future<proto::commute::rpc::jet::Request> auto parse = await::futures::Submit(executor_, [self = shared_from_this(), message]() {
    return self->TryParseRequest(message);
  });

  std::move(parse) | await::futures::Map([self = shared_from_this(), client](proto::commute::rpc::jet::Request request) {
    self->HandleRequest(request, client);
    return wheels::Unit{};
  }) | await::futures::Go();
}

void Server::HandleDisconnect(const std::string& /*client*/) {
  // Client disconnected
}

fallible::Result<proto::commute::rpc::jet::Request> Server::TryParseRequest(const transport::Message& message) {
  ::proto::commute::rpc::jet::Request wire_request;

  if (!pickle::Deserialize(message, &wire_request)) {
    TIMBER_LOG_WARN("Bad request from {}", "?");  // TODO

    return fallible::Fail(
        fallible::errors::Invalid()
          .Reason("Bad request").Done());
  }

  if (wire::ComputeChecksum(wire_request) != wire_request.checksum()) {
    WHEELS_PANIC("Request checksum mismatch");  // TODO: Can we recover?
  }

  return fallible::Ok(std::move(wire_request));
}

void Server::HandleRequest(const proto::commute::rpc::jet::Request& request,
                           const transport::ISocketPtr& client) {
  auto method = wire::GetMethod(request);
  auto context = wire::GetContext(request);
  auto body = request.body();

  auto self = shared_from_this();

  endpoint_->Call({method, body, context}) |
    await::futures::Apply([self, method, request, client](fallible::Result<ByteMessage> result) {
      self->Respond(request, std::move(result), client);
    }) |
    await::futures::Go();
}

void Server::Respond(const ::proto::commute::rpc::jet::Request& request,
                     fallible::Result<ByteMessage> result,
                     const transport::ISocketPtr& client) {
  if (result.Failed()) {
    RespondWithError(request, client, std::move(result.Error()));
    return;
  }

  // Ok
  ::proto::commute::rpc::jet::Response wire_response;
  wire::PrepareResponse(wire_response, request);
  wire::SetContext(wire_response, await::tasks::curr::Context());
  wire_response.set_body(std::move(*result));

  SendResponse(wire_response, client);
}

static bool IsTimeout(fallible::Error error) {
  return error.Code() == fallible::ErrorCodes::TimedOut;
}

void Server::RespondWithError(
    const ::proto::commute::rpc::jet::Request& wire_request,
    const transport::ISocketPtr& client, fallible::Error error) {
  if (IsTimeout(error)) {
    return;
  }

  ::proto::commute::rpc::jet::Response wire_response;
  wire::PrepareResponse(wire_response, wire_request);
  wire::SetContext(wire_response, await::tasks::curr::Context());
  wire::SetError(wire_response, error);
  SendResponse(wire_response, client);
}

void Server::SendResponse(::proto::commute::rpc::jet::Response& response,
                          const transport::ISocketPtr& client) {
  wire::FinalizeResponse(response);
  client->Send(pickle::Serialize(response));
}

}  // namespace commute::rpc::jet
