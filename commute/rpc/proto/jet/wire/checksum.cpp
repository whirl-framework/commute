#include <commute/rpc/proto/jet/wire/checksum.hpp>

#include <commute/support/checksum.hpp>

using commute::support::Checksum;

using ProtoHeaderValue = proto::commute::rpc::jet::header::Value;

namespace commute::rpc::jet::wire {

//////////////////////////////////////////////////////////////////////

static void AddHeader(Checksum& checksum, const ProtoHeaderValue& value) {
  if (value.has_str()) {
    checksum.Extend(value.str());
  } else if (value.has_uint()) {
    checksum.Extend(value.uint());
  } else if (value.has_flag()) {
    checksum.Extend(value.flag());
  } else {
    std::abort();  // Not supported
  }
}

//////////////////////////////////////////////////////////////////////

Checksum::Value ComputeChecksum(
    const proto::commute::rpc::jet::Request& request) {
  Checksum checksum;

  checksum.Extend(request.id());

  checksum.Extend(request.method().service());
  checksum.Extend(request.method().name());

  // Headers
  for (const auto& header : request.headers()) {
    checksum.Extend(header.name());
    AddHeader(checksum, header.value());
  }

  checksum.Extend(request.body());

  // Propagated context

  return checksum.GetValue();
}

Checksum::Value ComputeChecksum(
    const proto::commute::rpc::jet::Response& response) {
  Checksum checksum;

  checksum.Extend(response.request_id());
  checksum.Extend(response.trace_id());
  checksum.Extend(response.method().service());
  checksum.Extend(response.method().name());
  checksum.Extend((uint64_t)response.error().code());
  checksum.Extend(response.body());

  // Headers
  for (const auto& header : response.headers()) {
    checksum.Extend(header.name());
    AddHeader(checksum, header.value());
  }

  return checksum.GetValue();
}

}  // namespace commute::rpc::jet::wire
