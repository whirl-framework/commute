#include <commute/rpc/proto/jet/wire/context.hpp>

#include <commute/rpc/proto/jet/wire/header.hpp>
#include <commute/rpc/proto/jet/wire/method.hpp>

#include <commute/rpc/core/server/metadata.hpp>

#include <carry/new.hpp>

namespace commute::rpc::jet::wire {

//////////////////////////////////////////////////////////////////////

// Request

void SetContext(proto::commute::rpc::jet::Request& request,
                const carry::Context& context) {
  // Headers

  auto headers = headers::List(context);
  for (const auto& header : headers) {
    wire::SetHeader(request.mutable_headers()->Add(), header);
  }
}

carry::Context GetContext(const proto::commute::rpc::jet::Request& request) {
  carry::Builder builder;

  // Metadata

  {
    Metadata meta;
    meta.id = request.id();
    meta.peer = request.peer();
    meta.method = wire::GetMethod(request);

    builder.Set(MetadataKey(), std::move(meta));
  }

  // Headers

  for (const auto& proto : request.headers()) {
    auto header = wire::GetHeader(proto);
    builder.Set(headers::ContextKey(header.name), header.value);
  }

  return builder.Done();
}

//////////////////////////////////////////////////////////////////////

// Response

void SetContext(proto::commute::rpc::jet::Response& response,
                const carry::Context& context) {
  // Headers
  auto headers = headers::List(context);
  for (const auto& header : headers) {
    wire::SetHeader(response.mutable_headers()->Add(), header);
  }
}

carry::Context GetContext(
    const proto::commute::rpc::jet::Response& response) {
  carry::Builder builder;

  // Headers
  for (const auto& proto : response.headers()) {
    auto header = wire::GetHeader(proto);
    builder.Set(headers::ContextKey(header.name), header.value);
  }

  return builder.Done();
}

}  // namespace commute::rpc::jet::wire
