#pragma once

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

#include <carry/context.hpp>

namespace commute::rpc::jet::wire {

//////////////////////////////////////////////////////////////////////

// Request

void SetContext(proto::commute::rpc::jet::Request& request,
                const carry::Context& context);

carry::Context GetContext(
    const proto::commute::rpc::jet::Request& request);


//////////////////////////////////////////////////////////////////////

// Response

void SetContext(proto::commute::rpc::jet::Response& response,
                const carry::Context& context);

carry::Context GetContext(
    const proto::commute::rpc::jet::Response& response);

}  // namespace commute::rpc::jet::wire
