#include <commute/rpc/proto/jet/wire/error.hpp>

#include <fallible/error/make.hpp>

namespace commute::rpc::jet::wire {

//////////////////////////////////////////////////////////////////////

void SetError(proto::commute::rpc::jet::error::Error& proto, const fallible::Error error) {
  proto.set_code(error.Code());

  {
    // Context
    auto* proto_context = proto.mutable_context();

    proto_context->set_domain(error.Domain());
    proto_context->set_reason(error.Reason());

    {
      // Source location
      auto* proto_source = proto_context->mutable_source();

      proto_source->set_file(std::string(error.SourceLocation().File()));
      proto_source->set_function(std::string(error.SourceLocation().Function()));
      proto_source->set_line(error.SourceLocation().Line());
    }

    {
      // Attributes
      auto* proto_attrs = proto_context->mutable_attrs();
      for (const auto& [key, value] : error.Attrs()) {
        (*proto_attrs)[key] = value;
      }
    }
  }

  {
    // Sub-Errors
    auto* proto_sub_errors = proto.mutable_sub_errors();

    auto sub_errors = error.SubErrors();
    for (size_t i = 0; i < sub_errors.size(); ++i) {
      SetError(proto_sub_errors->at(i), sub_errors[i]);
    }
  }
}

void SetError(proto::commute::rpc::jet::Response& response,
              const fallible::Error& error) {
  SetError(*response.mutable_error(), error);
}

//////////////////////////////////////////////////////////////////////

fallible::Error LoadError(const proto::commute::rpc::jet::error::Error& proto) {
  fallible::detail::ErrorBuilder builder(proto.code(), wheels::SourceLocation::Current());

  {
    // Context
    builder.Domain(proto.context().domain());
    builder.Reason(proto.context().reason());

    {
      // Source location
      const auto& source = proto.context().source();
      builder.Location({source.file(), source.function(), source.line()});
    }

    {
      // Attributes
      for (const auto& [key, value] : proto.context().attrs()) {
        builder.Attr(key, value);
      }
    }
  }

  {
    // Sub-Errors
    for (const auto& proto : proto.sub_errors()) {
      builder.AddSubError(LoadError(proto));
    }
  }

  return builder.Done();
}

fallible::Error GetError(const proto::commute::rpc::jet::Response& response) {
  return LoadError(response.error());
}

//////////////////////////////////////////////////////////////////////

}  // namespace commute::rpc::jet::wire
