#pragma once

#include <commute/rpc/core/request/method.hpp>

#include <commute/rpc/proto/jet/proto/cpp/request.pb.h>

namespace commute::rpc::jet::wire {

inline Method GetMethod(const proto::commute::rpc::jet::Request& request) {
  return {request.method().service(), request.method().name()};
}

inline void SetMethod(proto::commute::rpc::jet::Method* proto, const Method& method) {
  proto->set_service(method.service);
  proto->set_name(method.name);
}

}  // namespace commute::rpc::jet::wire
