#include <commute/rpc/proto/jet/wire/response.hpp>

#include <commute/rpc/proto/jet/wire/checksum.hpp>

namespace commute::rpc::jet::wire {

void PrepareResponse(proto::commute::rpc::jet::Response& response,
                     const proto::commute::rpc::jet::Request& request) {
  // Mirror some fields
  response.set_request_id(request.id());
  // TODO: tracing?
  response.mutable_method()->CopyFrom(request.method());
}

void FinalizeResponse(proto::commute::rpc::jet::Response& response) {
  response.set_checksum(wire::ComputeChecksum(response));
}

}  // namespace commute::rpc::jet::wire
