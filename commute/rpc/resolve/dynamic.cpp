#include <commute/rpc/resolve/dynamic.hpp>

#include <commute/rt/locate/discovery.hpp>

namespace commute::rpc {

//////////////////////////////////////////////////////////////////////

class Resolver : public IResolver {
 public:
  Resolver(discovery::Path path)
    : path_(std::move(path)) {
  }

  Addrs Resolve() override {
    return rt::Discovery().Lookup(path_);
  }

 private:
  const discovery::Path path_;
};

//////////////////////////////////////////////////////////////////////

IResolverPtr Dynamic(discovery::Path path) {
  return std::make_shared<Resolver>(std::move(path));
}

}  // namespace commute::rpc
