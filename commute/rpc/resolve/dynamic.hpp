#pragma once

#include <commute/rpc/resolve/resolver.hpp>

#include <commute/discovery/path.hpp>

namespace commute::rpc {

IResolverPtr Dynamic(discovery::Path);

}  // namespace commute::rpc
