#pragma once

#include <commute/rpc/resolve/resolver.hpp>

namespace commute::rpc {

IResolverPtr Static(Addrs);

}  // namespace commute::rpc
