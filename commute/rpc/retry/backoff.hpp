#pragma once

#include <chrono>
#include <memory>

namespace commute::rpc::retry {

//////////////////////////////////////////////////////////////////////

// IRequestBackoff produces series of delays between request attempts

struct IRequestBackoff {
  virtual ~IRequestBackoff() = default;

  virtual std::chrono::milliseconds Next() = 0;
};

using IRequestBackoffPtr = std::unique_ptr<IRequestBackoff>;

//////////////////////////////////////////////////////////////////////

// Backoff strategy

struct IBackoff {
  virtual ~IBackoff() = default;

  virtual IRequestBackoffPtr NewRequest() = 0;
};

using IBackoffPtr = std::shared_ptr<IBackoff>;

}  // namespace commute::rpc::retry
