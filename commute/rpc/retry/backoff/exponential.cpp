#include <commute/rpc/retry/backoff/exponential.hpp>

#include <utility>
#include <algorithm>

using std::chrono::milliseconds;

namespace commute::rpc::retry::backoff {

//////////////////////////////////////////////////////////////////////

class ExpBackoff : public IRequestBackoff {
  using Millis = std::chrono::milliseconds;
 public:
  ExpBackoff(ExponentialParams params)
      : params_(params),
        next_(params.init) {
  }

  Millis Next() override {
    return std::exchange(next_, ComputeNext(next_));
  }

 private:
  Millis ComputeNext(Millis curr) const {
    return std::min<milliseconds>(curr * params_.factor, params_.max);
  }

 private:
  const ExponentialParams params_;
  Millis next_;
};

//////////////////////////////////////////////////////////////////////

class ExpBackoffStrategy : public IBackoff {
 public:
  ExpBackoffStrategy(ExponentialParams params)
      : params_(params) {
  }

  IRequestBackoffPtr NewRequest() override {
    return std::make_unique<ExpBackoff>(params_);
  }

 private:
  const ExponentialParams params_;
};

//////////////////////////////////////////////////////////////////////

IBackoffPtr Exponential(ExponentialParams params) {
  return std::make_shared<ExpBackoffStrategy>(params);
}

}  // namespace commute::rpc::retry::backoff
