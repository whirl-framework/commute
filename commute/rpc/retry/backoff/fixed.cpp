#include <commute/rpc/retry/backoff/fixed.hpp>

namespace commute::rpc::retry::backoff {

//////////////////////////////////////////////////////////////////////

class FixedDelay : public IRequestBackoff {
 public:
  explicit FixedDelay(std::chrono::milliseconds delay)
      : delay_(delay) {
  }

  std::chrono::milliseconds Next() override {
    return delay_;
  }
 private:
  const std::chrono::milliseconds delay_;
};

//////////////////////////////////////////////////////////////////////

class FixedDelayStrategy : public IBackoff {
 public:
  explicit FixedDelayStrategy(std::chrono::milliseconds delay)
      : delay_(delay) {
  }

  IRequestBackoffPtr NewRequest() override {
    return std::make_unique<FixedDelay>(delay_);
  }

 private:
  const std::chrono::milliseconds delay_;
};

//////////////////////////////////////////////////////////////////////

IBackoffPtr Fixed(std::chrono::milliseconds delay) {
  return std::make_shared<FixedDelayStrategy>(delay);
}

}  // namespace commute::rpc::retry::backoff
