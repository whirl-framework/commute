#include <commute/rpc/retry/backoff/jitter.hpp>

namespace commute::rpc::retry::backoff {

// Backoff + jitter

//////////////////////////////////////////////////////////////////////

class BackoffWithJitter : public IRequestBackoff {
  using Millis = std::chrono::milliseconds;
 public:
  BackoffWithJitter(IRequestBackoffPtr backoff, IJitterPtr jitter)
      : backoff_(std::move(backoff)), jitter_(std::move(jitter)) {
  }

  // IBackoff
  Millis Next() override {
    return jitter_->Add(backoff_->Next());
  }

 private:
  IRequestBackoffPtr backoff_;
  IJitterPtr jitter_;
};

//////////////////////////////////////////////////////////////////////

class WithJitterStrategy : public IBackoff {
 public:
  explicit WithJitterStrategy(IBackoffPtr backoff,
                              IJitterPtr jitter)
      : backoff_(std::move(backoff)), jitter_(std::move(jitter)) {
  }

  IRequestBackoffPtr NewRequest() override {
    return std::make_unique<BackoffWithJitter>(
        backoff_->NewRequest(), jitter_);
  }

 private:
  IBackoffPtr backoff_;
  IJitterPtr jitter_;
};

//////////////////////////////////////////////////////////////////////

IBackoffPtr WithJitter(
    IBackoffPtr backoff,
    IJitterPtr jitter) {
  return std::make_shared<WithJitterStrategy>(std::move(backoff), std::move(jitter));
}

}  // namespace commute::rpc::retry::backoff
