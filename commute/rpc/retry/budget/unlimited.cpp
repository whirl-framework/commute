#include <commute/rpc/retry/budget/unlimited.hpp>

namespace commute::rpc::retry::budget {

//////////////////////////////////////////////////////////////////////

struct UnlimitedBudget final : IBudget {
  bool TryWithdraw() override {
    return true;
  }

  void Deposit() override {
    // No-op
  }
};

//////////////////////////////////////////////////////////////////////

IBudgetPtr Unlimited() {
  return std::make_shared<UnlimitedBudget>();
}

}  // namespace commute::rpc::retry::budget
