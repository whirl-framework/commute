#pragma once

#include <commute/rpc/retry/budget.hpp>

namespace commute::rpc::retry::budget {

IBudgetPtr Unlimited();

}  // namespace commute::rpc::retry::budget
