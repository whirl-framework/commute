#pragma once

// Services
#include <snowflake/random/generator.hpp>

#include <chrono>
#include <memory>

namespace commute::rpc::retry {

//////////////////////////////////////////////////////////////////////

// Random jitter
// https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/

//////////////////////////////////////////////////////////////////////

struct IJitter {
  virtual ~IJitter() = default;

  virtual std::chrono::milliseconds Add(std::chrono::milliseconds delay) = 0;
};

using IJitterPtr = std::shared_ptr<IJitter>;

}  // namespace commute::rpc::retry
