#include <commute/rpc/retry/jitter/full.hpp>

#include <commute/rt/locate/random.hpp>

using Millis = std::chrono::milliseconds;

namespace commute::rpc::retry::jitter {

//////////////////////////////////////////////////////////////////////

class FullJitter : public IJitter {
 public:
  FullJitter(snowflake::random::IGenerator& random)
      : random_(random) {
  }

  // IJitter
  Millis Add(Millis delay) override {
    return Millis{random_.Jitter(delay.count())};
  }

 private:
  snowflake::random::IGenerator& random_;
};

//////////////////////////////////////////////////////////////////////

IJitterPtr Full(snowflake::random::IGenerator& random) {
  return std::make_shared<FullJitter>(random);
}

IJitterPtr Full() {
  return Full(rt::Random());
}

}  // namespace commute::rpc::retry::jitter
