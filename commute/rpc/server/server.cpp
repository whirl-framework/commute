#include <commute/rpc/server/server.hpp>

#include <commute/rpc/proto/jet/server/impl.hpp>

namespace commute::rpc {

IServerPtr MakeServer(std::string port) {
  return std::make_shared<jet::Server>(std::move(port));
}

}  // namespace commute::rpc
