#pragma once

#include <commute/rpc/core/client/channel.hpp>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

struct ICounter : IChannel {
  virtual size_t Count() const = 0;
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<ICounter> Count(IChannelPtr channel);

}  // namespace commute::rpc::test::channels
