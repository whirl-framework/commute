#pragma once

#include <commute/rpc/core/client/channel.hpp>

#include <chrono>

namespace commute::rpc::test::channels {

IChannelPtr Delay(IChannelPtr channel,
                  std::chrono::milliseconds millis);

}  // namespace commute::rpc::test::channels
