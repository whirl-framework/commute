#include <commute/rpc/test/channels/faulty.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <fallible/error/make.hpp>

#include <timber/log/log.hpp>

#include <system_error>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

class FaultyChannel : public rpc::IChannel {
 public:
  FaultyChannel(rpc::IChannelPtr underlying,
                IAdversaryPtr adversary)
      : underlying_(std::move(underlying)),
        adversary_(std::move(adversary)),
        logger_("Faulty-Channel") {
  }

  BoxedFuture<rpc::ByteMessage> Call(Request request) override {
    if (adversary_->Test(request)) {
      return Continue(std::move(request));
    } else {
      TIMBER_LOG_INFO("Fail request {}", request.method);
      return Fail(std::move(request));
    }
  }

  live::Status Status() override {
    return underlying_->Status();
  }

  const std::string& Peer() const override {
    return underlying_->Peer();
  }

  void Close() override {
    underlying_->Close();
  }

 private:
  BoxedFuture<ByteMessage> Continue(Request request) {
    return underlying_->Call(std::move(request));
  }

  BoxedFuture<ByteMessage> Fail(Request /*request*/) {
    return commute::futures::Fail<ByteMessage>(
        fallible::errors::Disconnected()
            .Domain(rpc::errors::Domain())
            .Reason(fmt::format("Adversary failed this request"))
            .Done());
  }

 private:
  rpc::IChannelPtr underlying_;
  IAdversaryPtr adversary_;
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

rpc::IChannelPtr Faulty(rpc::IChannelPtr channel,
                        IAdversaryPtr adversary) {
  return std::make_shared<FaultyChannel>(std::move(channel),
                                         std::move(adversary));
}

}  // namespace commute::rpc::test::channels
