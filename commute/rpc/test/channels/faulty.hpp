#pragma once

#include <commute/rpc/core/client/channel.hpp>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

struct IAdversary {
  virtual ~IAdversary() = default;

  virtual bool Test(const Request&) = 0;
};

using IAdversaryPtr = std::unique_ptr<IAdversary>;

//////////////////////////////////////////////////////////////////////

rpc::IChannelPtr Faulty(rpc::IChannelPtr channel,
                        IAdversaryPtr adversary);

}  // namespace commute::rpc::test::channels
