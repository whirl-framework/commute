#include <commute/rpc/test/channels/record.hpp>

#include <commute/rt/locate/clock.hpp>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

class Recorder : public IRecorder {
 public:
  Recorder(IChannelPtr channel)
    : channel_(std::move(channel)) {
  }

  // IChannel

  BoxedFuture<ByteMessage> Call(Request request) override {
    timestamps_.push_back(rt::WallClock().Now());
    return channel_->Call(std::move(request));
  }

  live::Status Status() override {
    return channel_->Status();
  }

  const std::string& Peer() const override {
    return channel_->Peer();
  }

  void Close() override {
    channel_->Close();
  }

  // IRecorder

  size_t CallCount() const override {
    return timestamps_.size();
  }

  std::vector<Timestamp> CallTimestamps() const override {
    return timestamps_;
  }

 private:
  IChannelPtr channel_;
  std::vector<Timestamp> timestamps_;
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<IRecorder> Record(IChannelPtr channel) {
  return std::make_shared<Recorder>(channel);
}

}  // namespace commute::rpc::test::channels
