#include <commute/rpc/test/channels/service.hpp>

#include <commute/rpc/errors/domain.hpp>

#include <commute/concurrency/futures/make/fail.hpp>

#include <fallible/error/error.hpp>
#include <fallible/error/codes.hpp>

#include <fmt/core.h>

namespace commute::rpc::test::channels {

//////////////////////////////////////////////////////////////////////

class ServiceChannel : public IChannel {
 public:
  ServiceChannel(IServicePtr service)
      : service_(std::move(service)) {
    service_->Initialize();
  }

  BoxedFuture<ByteMessage> Call(Request request) override {
    if (request.method.service != service_->Name()) {
      return futures::Fail<ByteMessage>(
          fallible::errors::NotFound()
              .Domain(rpc::errors::Domain())
              .Reason(fmt::format("Requested service `{}` not found",
                                  request.method.service))
              .Done());
    }

    if (!service_->Has(request.method.name)) {
      return futures::Fail<ByteMessage>(
          fallible::errors::NotFound()
              .Domain(rpc::errors::Domain())
              .Reason(
                  fmt::format("Requested method `{}` not found in service `{}`",
                              request.method.name, request.method.service))
              .Done());
    }

    return service_->Invoke(request.method.name, request.body);
  }

  live::Status Status() override {
    return live::Status::Alive;
  }

  const std::string& Peer() const override {
    static const std::string kPeer = "Test";
    return kPeer;
  }

  void Close() override {
    // Nop
  }

 private:
  IServicePtr service_;
};

//////////////////////////////////////////////////////////////////////

IChannelPtr Service(IServicePtr service) {
  return std::make_shared<ServiceChannel>(std::move(service));
}

}  // namespace commute::rpc::test::channels
