#pragma once

#include <commute/rpc/core/client/channel.hpp>
#include <commute/rpc/core/service/service.hpp>

namespace commute::rpc::test::channels {

IChannelPtr Service(IServicePtr service);

}  // namespace commute::rpc::test::channels
