#include <commute/rpc/test/filters/count.hpp>

#include <atomic>

namespace commute::rpc::test::filters {

//////////////////////////////////////////////////////////////////////

class Counter : public ICounter {
 public:
  Counter(IEndpointPtr e)
      : e_(std::move(e)) {
  }

  // IEndpoint

  BoxedFuture<ByteMessage> Call(Request request) override {
    ++count_;
    return e_->Call(std::move(request));
  }

  const std::string& Name() const override {
    return e_->Name();
  }

  // ICounter

  size_t Count() const override {
    return count_.load();
  }

 private:
  IEndpointPtr e_;

  std::atomic<size_t> count_{0};
};

//////////////////////////////////////////////////////////////////////

std::shared_ptr<ICounter> Count(IEndpointPtr e) {
  return std::make_shared<Counter>(std::move(e));
}

}  // namespace commute::rpc::test::filters
