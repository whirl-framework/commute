#pragma once

#include <ticktock/clocks/wall_clock.hpp>

namespace commute::rt {

ticktock::IWallClock& WallClock();

}  // namespace commute::rt
