#pragma once

#include <commute/discovery/discovery.hpp>

namespace commute::rt {

discovery::IDiscovery& Discovery();

}  // namespace commute::rt
