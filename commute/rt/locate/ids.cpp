#include <commute/rt/locate/ids.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

snowflake::id::IGenerator& Ids() {
  return compass::Locate<snowflake::id::IGenerator>();
}

}  // namespace commute::rt
