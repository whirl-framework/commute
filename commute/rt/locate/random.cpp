#include <commute/rt/locate/random.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

snowflake::random::IGenerator& Random() {
  return compass::Locate<snowflake::random::IGenerator>();
}

}  // namespace commute::rt
