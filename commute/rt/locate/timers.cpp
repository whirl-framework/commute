#include <commute/rt/locate/timers.hpp>

#include <compass/locate.hpp>

namespace commute::rt {

await::timers::ITimerKeeper& Timers() {
  return compass::Locate<await::timers::ITimerKeeper>();
}

}  // namespace commute::rt
