#pragma once

#include <await/timers/core/timer_keeper.hpp>

namespace commute::rt {

await::timers::ITimerKeeper& Timers();

}  // namespace commute::rt
