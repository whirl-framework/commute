#pragma once

#include <commute/transport/transport.hpp>

namespace commute::rt {

transport::ITransport& Transport();

}  // namespace commute::rt
