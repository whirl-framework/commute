#pragma once

#include <ticktock/clocks/wall_clock.hpp>

#include <cassert>
#include <chrono>

namespace commute::matrix {

// Virtual time

class Clock : public ticktock::IWallClock {
 public:
  using TimePoint = ticktock::WallTime;
  using Duration = std::chrono::microseconds;

 public:
  // IWallClock
  TimePoint Now() const override {
    return now_;
  }

  void FastForwardTo(TimePoint future) {
    assert(future >= now_);
    now_ = future;
  }

 private:
  TimePoint now_{std::chrono::milliseconds{0}};
};

}  // namespace commute::matrix
