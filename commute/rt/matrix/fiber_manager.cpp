#include <commute/rt/matrix/fiber_manager.hpp>

namespace commute::matrix {

wheels::MutableMemView FiberManager::AllocateStack() {
  return {(char*)std::malloc(stack_size_), stack_size_};
}

void FiberManager::ReleaseStack(wheels::MutableMemView stack) {
  std::free(stack.Data());
}

}  // namespace commute::matrix
