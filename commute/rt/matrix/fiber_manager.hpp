#pragma once

#include <await/fibers/core/resource_manager.hpp>

namespace commute::matrix {

class FiberManager : public await::fibers::IResourceManager {
 public:
  // Stacks
  void SetStackSize(size_t bytes) override {
    stack_size_ = bytes;
  }

  wheels::MutableMemView AllocateStack() override;
  void ReleaseStack(wheels::MutableMemView stack) override;

  // Ids
  await::fibers::FiberId GenerateId() override {
    return ++next_id_;
  }

 private:
  static const size_t kDefaultStackSize = 64 * 1024;

 private:
  size_t next_id_ = 0;
  size_t stack_size_ = kDefaultStackSize;
};

}  // namespace commute::matrix
