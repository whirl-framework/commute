#pragma once

#include <snowflake/id/generator.hpp>

#include <fmt/core.h>

namespace commute::matrix {

class IdGenerator : public snowflake::id::IGenerator {
 public:
  snowflake::id::Id GenerateId() override {
    return fmt::format("ID-{}", ++count_);
  }

 private:
  size_t count_ = 0;
};

}  // namespace commute::matrix
