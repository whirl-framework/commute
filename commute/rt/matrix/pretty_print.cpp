#include <commute/rt/matrix/pretty_print.hpp>

#include <pickle/serialize.hpp>

#include <sstream>

#include <fmt/core.h>

namespace commute::matrix {

std::string PrintBytes(const std::string& bytes) {
  std::stringstream out;
  out << "<bytes(" << bytes.length() << ")>";
  return out.str();
}

std::string PrettyPrint(const std::string& message) {
  if (pickle::Format::IsBinary()) {
    return PrintBytes(message);
  } else {
    // Probably json
    return message;
  }
}

std::string PrettyPrint(const std::chrono::nanoseconds nanos) {
  if (nanos.count() % 1000'000'000 == 0) {
    return fmt::format("{}s", nanos.count() / 1000'000'000);
  }
  if (nanos.count() % 1000'000 == 0) {
    return fmt::format("{}ms", nanos.count() / 1000'000);
  }
  if (nanos.count() % 1000 == 0) {
    return fmt::format("{}us", nanos.count() / 1000);
  }
  return fmt::format("{}ns", nanos.count());
}

}  // namespace commute::matrix
