#pragma once

#include <string>
#include <chrono>

namespace commute::matrix {

std::string PrettyPrint(const std::string& message);

std::string PrettyPrint(const std::chrono::nanoseconds nanos);

}  // namespace commute::matrix
