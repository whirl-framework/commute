#include <commute/rt/matrix/runtime.hpp>

#include <commute/rt/matrix/pretty_print.hpp>

#include <commute/support/fmt.hpp>

#include <timber/log/log.hpp>

#include <compass/map.hpp>

namespace commute::matrix {

Runtime::Runtime(std::string name)
    : name_(name),
      log_(clock_),
      twister_(42),
      tracer_(ids_, clock_, log_),
      executor_(fiber_manager_),
      timers_(clock_, executor_),
      transport_(executor_, twister_, clock_, log_),
      logger_("Matrix", log_) {

  // Setup
  compass::Map()
      .Add<await::tasks::IExecutor>(*this)
      .Add<await::timers::ITimerKeeper>(*this)
      .Add<ticktock::IWallClock>(*this)
      .Add<snowflake::random::IGenerator>(*this)
      .Add<snowflake::id::IGenerator>(*this)
      .Add<timber::log::IBackend>(*this)
      .Add<timber::trace::ITracer>(*this)
      .Add<commute::transport::ITransport>(*this)
      .Add<commute::discovery::IDiscovery>(*this);
}

Runtime::~Runtime() {
  //compass::Registry().Clear();
}

std::optional<Clock::TimePoint> Runtime::NextEvent() {
  std::optional<Clock::TimePoint> next;

  // Check timers
  if (timers_.HasTimers()) {
    next = timers_.NextDeadLine();
  }

  // Check network
  if (transport_.HasPackets()) {
    auto next_delivery_time = transport_.NextDeliveryTime();
    if (!next || (next_delivery_time < *next)) {
      next = next_delivery_time;
    }
  }

  return next;
}

void Runtime::Run() {
  Prologue();
  Drain();
  Stop();
  Drain();
  Epilogue();
}

void Runtime::Prologue() {
  // Set default stack size for fibers
  fiber_manager_.SetStackSize(1 * 1024 * 1024);

  TIMBER_LOG_INFO("Simulation '{}' started", name_);
}

void Runtime::Stop() {
  for (auto& f : actors_) {
    std::move(f).RequestCancel();
  }
  actors_.clear();
}

void Runtime::Drain() {
  while (executor_.HasTasks() || transport_.HasPackets() ||
         timers_.HasTimers()) {
    // Instant actions
    while (executor_.HasTasks() || transport_.Ready()) {
      executor_.RunAtMost(5);
      transport_.Run();
    }

    auto next_event = NextEvent();

    // Fast-forward
    if (next_event) {
      auto now = clock_.Now();

      auto till_next_event = *next_event - now;
      TIMBER_LOG_INFO("Fast-forward clock by {} to {}",
                      PrettyPrint(till_next_event), *next_event);

      clock_.FastForwardTo(*next_event);
      timers_.CompleteReadyTimers();
    }
  }
}

void Runtime::Epilogue() {
  TIMBER_LOG_INFO("Simulation completed");
}

}  // namespace commute::matrix
