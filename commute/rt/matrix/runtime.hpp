#pragma once

#include <compass/locator.hpp>

#include <commute/rt/matrix/clock.hpp>
#include <commute/rt/matrix/log.hpp>
#include <commute/rt/matrix/tracer.hpp>
#include <commute/rt/matrix/ids.hpp>
#include <commute/rt/matrix/timers.hpp>
#include <commute/rt/matrix/transport.hpp>
#include <commute/rt/matrix/discovery.hpp>
#include <commute/rt/matrix/tester.hpp>

#include <commute/rt/matrix/fiber_manager.hpp>
#include <await/tasks/exe/fibers/manual.hpp>
#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/types/eager.hpp>

#include <snowflake/random/impl/twister.hpp>

#include <list>

namespace commute::matrix {

class Runtime :
  public compass::Locator<ticktock::IWallClock>,
  public compass::Locator<snowflake::id::IGenerator>,
  public compass::Locator<snowflake::random::IGenerator>,
  public compass::Locator<await::tasks::IExecutor>,
  public compass::Locator<await::timers::ITimerKeeper>,
  public compass::Locator<timber::log::IBackend>,
  public compass::Locator<timber::trace::ITracer>,
  public compass::Locator<commute::discovery::IDiscovery>,
  public compass::Locator<commute::transport::ITransport> {

 public:
  Runtime(std::string name);
  ~Runtime();

  template <typename F>
  Runtime& Spawn(F&& f) {
    auto actor = await::futures::Submit(executor_, std::forward<F>(f)) | await::futures::Start();
    actors_.push_back(std::move(actor));
    return *this;
  }

  void Run();

  // Services

  ticktock::IWallClock& WallClock() {
    return clock_;
  }

  snowflake::id::IGenerator& Ids() {
    return ids_;
  }

  snowflake::random::IGenerator& Random() {
    return twister_;
  }

  transport::ITransport& Transport() {
    return transport_;
  }

  discovery::IDiscovery& Discovery() {
    return discovery_;
  }

  await::tasks::IExecutor& Executor() {
    return executor_;
  }

  await::timers::ITimerKeeper& Timers() {
    return timers_;
  }

  timber::log::IBackend& LogBackend() {
    return log_;
  }

  timber::trace::ITracer& Tracer() {
    return tracer_;
  }

  // Locators

  ticktock::IWallClock* Locate(compass::Tag<ticktock::IWallClock>) override {
    return &clock_;
  }

  snowflake::id::IGenerator* Locate(compass::Tag<snowflake::id::IGenerator>) override {
    return &ids_;
  }

  snowflake::random::IGenerator* Locate(compass::Tag<snowflake::random::IGenerator>) override {
    return &twister_;
  }

  transport::ITransport* Locate(compass::Tag<transport::ITransport>) override {
    return &transport_;
  }

  discovery::IDiscovery* Locate(compass::Tag<discovery::IDiscovery>) override {
    return &discovery_;
  }

  matrix::Discovery& DiscoveryImpl() {
    return discovery_;
  }

  await::tasks::IExecutor* Locate(compass::Tag<await::tasks::IExecutor>) override {
    return &executor_;
  }

  await::timers::ITimerKeeper* Locate(compass::Tag<await::timers::ITimerKeeper>) override {
    return &timers_;
  }

  timber::log::IBackend* Locate(compass::Tag<timber::log::IBackend>) override {
    return &log_;
  }

  matrix::LogBackend& LogBackendImpl() {
    return log_;
  }

  timber::trace::ITracer* Locate(compass::Tag<timber::trace::ITracer>) override {
    return &tracer_;
  }

  // Testing

  Tester& Tester() {
    return tester_;
  }

  void SetLatency(matrix::Latency latency) {
    transport_.SetLatency(latency);
  }

 private:
  void Prologue();
  void Drain();
  void Stop();
  void Epilogue();

  std::optional<Clock::TimePoint> NextEvent();

 private:
  const std::string name_;

  matrix::Clock clock_;
  matrix::LogBackend log_;
  snowflake::random::Twister twister_;
  matrix::IdGenerator ids_;
  matrix::Tracer tracer_;
  matrix::FiberManager fiber_manager_;
  await::tasks::fibers::ManualExecutor executor_;
  matrix::TimerService timers_;
  matrix::LocalTransport transport_;
  matrix::Discovery discovery_;

  timber::log::Logger logger_;

  class Tester tester_;

  using ActorFuture = await::futures::EagerUnitFuture;

  std::list<ActorFuture> actors_;
};

}  // namespace commute::matrix
