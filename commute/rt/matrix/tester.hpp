#pragma once

#include <set>
#include <string>

namespace commute::matrix {

class Tester {
  using CheckpointName = std::string;
 public:
  void Checkpoint(CheckpointName name) {
    checkpoints_.insert(std::move(name));
  }

  bool Reached(const CheckpointName& name) const {
    return checkpoints_.contains(name);
  }

 private:
  std::set<CheckpointName> checkpoints_;
};

}  // namespace commute::matrix
