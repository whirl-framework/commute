#include <commute/rt/matrix/timers.hpp>

namespace commute::matrix {

void TimerService::AddTimer(await::timers::TimerBase* timer) {
  auto deadline = clock_.ToDeadline(timer->Delay());

  timers_.push({deadline, /*handler=*/timer});
}

TimerService::HandlerList TimerService::GrabReadyTimers() {
  auto now = clock_.Now();

  HandlerList handlers;

  while (!timers_.empty()) {
    auto& next = timers_.top();
    if (next.deadline > now) {
      break;
    }

    handlers.PushBack(next.handler);

    timers_.pop();
  }

  return handlers;
}

size_t TimerService::CompleteReadyTimers() {
  HandlerList handlers = GrabReadyTimers();

  size_t count = 0;

  while (!handlers.IsEmpty()) {
    ++count;
    await::timers::ITimerHandler* handler = handlers.PopFront();
    handler->Alarm();
  }

  return count;
}

}  // namespace commute::matrix
