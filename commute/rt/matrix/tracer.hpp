#pragma once

#include <timber/trace/tracer.hpp>

#include <snowflake/id/generator.hpp>

#include <ticktock/clocks/wall_clock.hpp>

#include <timber/log/logger.hpp>

namespace commute::matrix {

class Tracer;

//////////////////////////////////////////////////////////////////////

class Span : public timber::trace::impl::Span {
 public:
  Span(Tracer& tracer, std::string name, timber::trace::References refs);

  void Finish() override;

  void Cancel() override;

  const std::string& Name() const override {
    return name_;
  }

  timber::trace::SpanContext Context() const override {
    return {trace_id_, span_id_};
  }

  timber::trace::Id TraceId() const override {
    return trace_id_;
  }

  timber::trace::Id SpanId() const override {
    return span_id_;
  }

  const timber::trace::References& References() const override {
    return refs_;
  }

  ticktock::WallTime StartTime() const {
    return start_time_;
  }

 private:
  void Init();

 private:
  Tracer& tracer_;

  const std::string name_;

  timber::trace::Id trace_id_;
  timber::trace::Id span_id_;

  timber::trace::References refs_;

  ticktock::WallTime start_time_;
};

//////////////////////////////////////////////////////////////////////

class Tracer : public timber::trace::ITracer {
 public:
  Tracer(snowflake::id::IGenerator& ids,
         ticktock::IWallClock& wall_clock,
         timber::log::IBackend& log)
    : ids_(ids),
      wall_clock_(wall_clock),
      logger_("Tracer", log) {
  }

  timber::trace::Span StartSpan(std::string name, timber::trace::References refs) override;

  timber::trace::Id GetTraceId(const timber::trace::References& refs);
  timber::trace::Id GenerateSpanId();

  ticktock::WallTime Now() {
    return wall_clock_.Now();
  }

  void Finish(const Span& span);
  void Cancel(const Span& span);

 private:
  snowflake::id::IGenerator& ids_;
  ticktock::IWallClock& wall_clock_;
  timber::log::Logger logger_;
};

}  // namespace commute::matrix
