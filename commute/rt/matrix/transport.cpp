#include <commute/rt/matrix/runtime.hpp>

#include <commute/rt/matrix/pretty_print.hpp>

#include <timber/log/log.hpp>

namespace commute::matrix {

transport::IServerPtr LocalTransport::Serve(
    const std::string& address,
    transport::IHandlerPtr handler) {
  auto server_port = std::atoi(address.c_str());
  assert(endpoints_.find(server_port) == endpoints_.end());

  endpoints_[server_port] = {handler};

  return std::make_shared<Server>(server_port, this);
}

transport::ISocketPtr LocalTransport::ConnectTo(
    const std::string& address,
    transport::IHandlerPtr handler) {
  auto server_port = std::atoi(address.c_str());
  auto port = FindNextFreePort();

  auto socket = std::make_shared<Socket>(port, server_port, this);

  endpoints_[port] = {handler};

  return socket;
}

size_t LocalTransport::Run() {
  size_t count = 0;
  while (packets_.Ready()) {
    Packet packet = packets_.PopFront();
    Handle(packet);
    ++count;
  }
  return count;
}

Port LocalTransport::FindNextFreePort() {
  while (true) {
    Port port = ++next_port_;
    if (!endpoints_.contains(port)) {
      return port;
    }
  }
}

void LocalTransport::Disconnect(Port from, Port to) {
  TIMBER_LOG_INFO("Disconnect port {} from port {}", from, to);
  auto it = endpoints_.find(from);
  if (it != endpoints_.end()) {
    Endpoint& sender = it->second;
    HandleDisconnect(sender.handler, fmt::format("localhost:{}", to));
  }
}

void LocalTransport::Send(Packet packet) {
  TIMBER_LOG_INFO("Send packet from port {} to port {}", packet.from, packet.to);
  packets_.Push(packet, PacketDelay());
}

void LocalTransport::Handle(Packet packet) {
  TIMBER_LOG_INFO("Handle packet from port {} to port {}", packet.from, packet.to);

  auto it = endpoints_.find(packet.to);
  if (it == endpoints_.end()) {
    Disconnect(packet.from, packet.to);
    return;
  }
  Endpoint& receiver = it->second;

  auto reply_socket =
      std::make_shared<Socket>(packet.to, packet.from, this, true);

  TIMBER_LOG_INFO("Handle message {} at port {}", PrettyPrint(packet.message), packet.to);
  HandleMessage(receiver.handler, reply_socket, packet.message);
}

}  // namespace commute::matrix
