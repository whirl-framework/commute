#include <commute/support/checksum.hpp>

#include <crc32c/crc32c.h>

namespace commute::support {

void Checksum::ExtendImpl(const uint8_t* data, size_t count) {
  accumulated_value_ = crc32c::Extend(accumulated_value_, data, count);
}

}  // namespace commute::support
