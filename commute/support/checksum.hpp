#pragma once

#include <cstdint>
#include <string>

namespace commute::support {

class Checksum {
 public:
  using Value = uint32_t;

  Checksum() = default;

  void Extend(const std::string& data) {
    ExtendImpl((const uint8_t*)data.data(), data.length());
  }

  void Extend(uint64_t value) {
    ExtendImpl((const uint8_t*)&value, 8);
  }

  void Extend(bool value) {
    Extend((uint64_t)(value ? 1 : 0));
  }

  Value GetValue() const {
    return accumulated_value_;
  }

 private:
  void ExtendImpl(const uint8_t* data, size_t count);

 private:
  Value accumulated_value_ = 0;
};

}  // namespace commute::support
