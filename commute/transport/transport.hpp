#pragma once

#include <string>
#include <memory>

namespace commute::transport {

//////////////////////////////////////////////////////////////////////

using Message = std::string;

//////////////////////////////////////////////////////////////////////

struct ISocket {
  virtual ~ISocket() = default;

  virtual const std::string& Peer() const = 0;
  virtual void Send(const Message& message) = 0;
  virtual void Close() = 0;
  virtual bool IsConnected() const = 0;
};

using ISocketPtr = std::shared_ptr<ISocket>;

//////////////////////////////////////////////////////////////////////

struct IHandler {
  virtual ~IHandler() = default;

  virtual void HandleMessage(const Message& message,
                             ISocketPtr back) = 0;
  virtual void HandleDisconnect(const std::string& peer) = 0;
};

using IHandlerPtr = std::weak_ptr<IHandler>;

//////////////////////////////////////////////////////////////////////

struct IServer {
  virtual ~IServer() = default;

  virtual std::string Port() const = 0;
  virtual void Shutdown() = 0;
};

using IServerPtr = std::shared_ptr<IServer>;

//////////////////////////////////////////////////////////////////////

struct ITransport {
  virtual ~ITransport() = default;

  virtual const std::string& HostName() const = 0;
  virtual IServerPtr Serve(const std::string& port, IHandlerPtr handler) = 0;
  virtual ISocketPtr ConnectTo(const std::string& address,
                               IHandlerPtr handler) = 0;
};

}  // namespace commute::transport
