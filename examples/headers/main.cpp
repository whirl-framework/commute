#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/filters/furnish.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/await.hpp>

#include <carry/current.hpp>

#include <fallible/error/make.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/greeting.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class GreetingService final :
    public rpc::ServiceBase<GreetingService> {
 public:
  static constexpr std::string_view kName = "Greeting";

  explicit GreetingService()
    : logger_(kName) {
  }

  void Greet(const proto::greeting::Request& request,
             Response<proto::greeting::Response>& response) {
    TIMBER_LOG_INFO("Greeting request from {}", request.name());

    if (!Authorize(await::tasks::curr::Context())) {
      response.Fail(
          fallible::errors::Unauthorized()
            .Reason("Invalid or missing secret")
            .Done());
      return;
    }

    response->set_greeting(
        fmt::format("Hello, {}!", request.name()));
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

  static bool Authorize(const carry::Context& context) {
    auto secret = rpc::headers::TryGet(context, "client.secret");
    return secret.has_value() && (secret->AsString() == "Sesame");
  }

 private:
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto greeting = rpc::Make<GreetingService>();

  server->Serve(rpc::filters::Furnish(std::move(greeting)));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client() {
  timber::log::Logger logger_("Client");

  auto dialer = rpc::MakeDialer();
  auto socket = dialer->Dial(kServerPort);

  proto::greeting::Request hi;
  hi.set_name("Jake");

  auto future = rpc::Call("Greeting.Greet")
      .Request(hi)
      .Via(socket)
      .Header("client.secret", rpc::headers::Value::String("Sesame"))
      .Done()
      .As<proto::greeting::Response>();

  auto rsp = await::Await(std::move(future)).ExpectValue();

  std::cout << "Greeting: " << rsp.greeting() << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  matrix::Runtime sim("HeadersExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
