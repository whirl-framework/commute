#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>

#include <await/fibers/sched/self.hpp>
#include <await/await.hpp>

#include <fallible/error/make.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/arithm.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

// Service

class Calculator final :
    public rpc::ServiceBase<Calculator> {
 public:
  static constexpr std::string_view kName = "Calculator";

  void Add(const proto::arithm::BinaryOp& request,
           Response<proto::arithm::Result>& response) {
    // Access response message via -> operator
    response->set_value(request.lhs() + request.rhs());
  }

  void Multiply(const proto::arithm::BinaryOp& request,
                Response<proto::arithm::Result>& response) {
    assert(await::fibers::AmIFiber());
    response->set_value(request.lhs() * request.rhs());
  }

  void Divide(const proto::arithm::BinaryOp& request,
              Response<proto::arithm::Result>& response) {
    if (request.rhs() == 0) {
      response.Fail(
          fallible::errors::Invalid()
            .Reason("Cannot divide by 0")
            .Done());
      return;
    }

    response->set_value(request.lhs() / request.rhs());
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Add);
    COMMUTE_RPC_REGISTER_METHOD(Multiply);
    COMMUTE_RPC_REGISTER_METHOD(Divide);
  }
};

//////////////////////////////////////////////////////////////////////

void Server() {
  assert(await::fibers::AmIFiber());

  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto calculator = rpc::Make<Calculator>();

  // One-shot
  server->Serve(calculator);

  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client() {
  assert(await::fibers::AmIFiber());

  timber::log::Logger logger_("Client");

  // Client stack
  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(kServerPort);
    return socket;
  }();

  {
    // Request message
    proto::arithm::BinaryOp mult;
    mult.set_lhs(2);
    mult.set_rhs(2);

    // await::futures::Future
    auto future = rpc::Call("Calculator.Multiply")
        .Request(mult)
        .Via(stack)
        .Done()
        .As<proto::arithm::Result>();

    // fallible::Result
    auto result = await::Await(std::move(future));

    auto computed = result.ValueOrThrow();

    TIMBER_LOG_INFO("Computed = {}", computed.value());
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution
  matrix::Runtime sim("CalculatorExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
