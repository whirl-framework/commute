#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/observe/trace.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sched/self.hpp>
#include <await/await.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/greeting.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kProxyPort = "41";
static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class GreetingService final :
    public rpc::ServiceBase<GreetingService> {
 public:
  static constexpr std::string_view kName = "Greeting";

  GreetingService()
    : logger_(kName) {
  }

  void Greet(const proto::greeting::Request& request,
          Response<proto::greeting::Response>& response) {
    response->set_greeting(
        fmt::format("Hello, {}!", request.name()));
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Greet);
  }

 private:
  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto stack = [] {
    auto greeting = rpc::Make<GreetingService>();
    auto log = rpc::filters::Log(std::move(greeting));
    return log;
  }();

  server->Serve(std::move(stack));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Proxy() {
  auto server = rpc::MakeServer(kProxyPort);

  auto dialer = rpc::MakeDialer();
  auto socket = dialer->Dial(kServerPort);

  // Proxy: channel is-a endpoint
  server->Serve(std::move(socket));

  commute::fibers::SleepFor(100500s);

  server->Shutdown();
}

//////////////////////////////////////////////////////////////////////

void Client() {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(kProxyPort);

    auto trace = rpc::channels::Trace(std::move(socket));

    return trace;
  }();

  proto::greeting::Request hi;
  hi.set_name("Tilda");

  auto future = rpc::Call("Greeting.Greet")
      .Request(hi)
      .Via(stack)
      .Done()
      .As<proto::greeting::Response>();

  auto result = await::Await(std::move(future));

  assert(result.IsOk());

  TIMBER_LOG_INFO("Greeting: {}", result->greeting());

  TIMBER_LOG_INFO("Client completed");
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("ProxyExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Proxy();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
