#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/parking.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

static const size_t kConcurrencyLimit = 2;

//////////////////////////////////////////////////////////////////////

class Parking final :
    public rpc::ServiceBase<Parking> {
 public:
  static constexpr std::string_view kName = "Parking";

  void Park(const proto::parking::Request& /*request*/,
            Response<proto::parking::Response>& /*response*/) {
    size_t load = load_.fetch_add(1) + 1;
    assert(load <= kConcurrencyLimit);

    // Park current handler for 1s
    commute::fibers::SleepFor(1s);

    load_.fetch_sub(1);
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Park);
  }

 private:
  std::atomic<size_t> load_{0};
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  auto stack = [] {
    auto parking = rpc::Make<Parking>();

    auto semaphore = rpc::filters::Semaphore(
        std::move(parking),
        rpc::filters::SemaphoreParams()
            .ConcurrencyLimit(kConcurrencyLimit)
            .QueueLimit(2));

    auto log = rpc::filters::Log(std::move(semaphore));

    return log;
  }();

  server->Serve(std::move(stack));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

// Backoff strategy for retry

rpc::retry::IBackoffPtr Backoff() {
  return rpc::retry::backoff::WithJitter(
      rpc::retry::backoff::Exponential({1s, 10s, 2}),
      rpc::retry::jitter::Equal());
}

void Client() {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    auto dialer = rpc::MakeDialer();
    auto socket = dialer->Dial(/*address=*/kServerPort);

    auto retry = rpc::channels::Retry(std::move(socket), Backoff());

    return retry;
  }();

  await::fibers::WaitGroup wg;

  for (size_t i = 0; i < 5; ++i) {
    proto::parking::Request req;

    auto future = rpc::Call("Parking.Park")
        .Request(req)
        .Via(stack)
        .Done()
        .As<proto::parking::Response>();

    wg.Add(std::move(future));
  }

  wg.Wait();
}

//////////////////////////////////////////////////////////////////////

int main() {
  // Deterministic execution!
  matrix::Runtime sim("SemaphoreExample");

  sim.Spawn([] {
    Server();
  }).Spawn([] {
    Client();
  });

  sim.Run();

  return 0;
}
