#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>

#include <commute/rpc/channels/balance/random.hpp>
#include <commute/rpc/channels/balance/rr.hpp>
#include <commute/rpc/channels/balance/p2c.hpp>

#include <commute/rpc/balance/load/ema.hpp>
#include <commute/rpc/balance/load/inflight.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/rpc/test/filters/count.hpp>

#include <commute/rpc/resolve/dynamic.hpp>

#include <commute/rpc/test/channels/count.hpp>

#include <await/fibers/sched/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>

#include <await/futures/make/after.hpp>
#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/flatten.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class Ponger : public rpc::ServiceBase<Ponger> {
 public:
  static constexpr const char *kName = "Ponger";

  explicit Ponger(std::chrono::milliseconds delay)
      : delay_(delay) {
  }

  void Ping(const proto::ping::Request & /*request*/,
            Response<proto::ping::Response> & response) {

    response.Set(await::futures::After(delay_) | await::futures::Map([](wheels::Unit) {
      return proto::ping::Response{};  // Pong
    }) | await::futures::Box());
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }

 private:
  std::chrono::milliseconds delay_;
};

//////////////////////////////////////////////////////////////////////

void Server(std::string port, std::chrono::milliseconds delay, matrix::Runtime& rt) {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(port);
  rt.DiscoveryImpl().Add("replicas", port);

  auto ponger = rpc::Make<Ponger>(delay);

  auto counter = rpc::test::filters::Count(ponger);

  server->Serve(
      rpc::filters::Log(
          counter));

  // Work for some time
  await::fibers::SleepFor(100500s);

  server->Shutdown();

  std::cout << "Server(" << port << "): " << counter->Count() << std::endl;

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client(matrix::Runtime& rt) {
  timber::log::Logger logger_("Client");

  auto stack = [] {
    auto dialer = rpc::MakeDialer();

    //auto ema_params = rpc::balance::load::EmaParams()
    //    .Window(5s);

    auto balancer = rpc::channels::PowerOf2Choices(
        rpc::Dynamic({"replicas"}),
        std::move(dialer),
        rpc::balance::load::InFlight());

    auto retry = rpc::channels::Retry(balancer, rpc::retry::backoff::Fixed(1s));

    return retry;
  }();

  await::fibers::WaitGroup wg;

  static const size_t kCalls = 12345;

  for (size_t i = 0; i < kCalls; ++i) {
    auto delay = std::chrono::milliseconds(rt.Random().Jitter(10000));

    auto call = await::futures::After(delay) | await::futures::Map([stack]() {
      return rpc::Call("Ponger.Ping")
          .Request(proto::ping::Request{})
          .Via(stack)
          .Done()
          .As<proto::ping::Response>();
    }) | await::futures::Flatten();

    wg.Add(std::move(call));
  }

  wg.Wait();
}

void Sim() {
  matrix::Runtime sim("Balance");

  sim.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  // Replicas

  // Fast
  for (size_t i = 0; i < 5; ++i) {
    sim.Spawn([&, i]() {
      Server(std::to_string(i), 50ms, sim);
    });
  }

  // Slow

  sim.Spawn([&]() {
    Server("6", 100ms, sim);
  });

  sim.Spawn([&]() {
    Server("7", 200ms, sim);
  });

  // Client

  sim.Spawn([&]() {
    Client(sim);
  });

  sim.Run();
}

int main() {
  Sim();
  return 0;
}
