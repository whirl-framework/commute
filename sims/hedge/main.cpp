#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/channels/failsafe/hedge.hpp>
#include <commute/rpc/channels/balance/random.hpp>

#include <commute/rpc/retry/backoff/fixed.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/rpc/resolve/dynamic.hpp>

#include <commute/rpc/test/channels/count.hpp>

#include <await/fibers/sched/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>
#include <await/futures/combine/seq/map.hpp>

#include <timber/log/log.hpp>

#include "proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class Ponger :
    public rpc::ServiceBase<Ponger> {
 public:
  static constexpr const char* kName = "Ponger";

  explicit Ponger(std::chrono::milliseconds delay)
      : delay_(delay) {
  }

  void Ping(const proto::ping::Request& /*request*/,
            Response<proto::ping::Response>& /*response*/) {
    await::fibers::SleepFor(delay_);
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }

 private:
  std::chrono::milliseconds delay_;
};

//////////////////////////////////////////////////////////////////////

void Server(std::string port, std::chrono::milliseconds delay, matrix::Runtime& rt) {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(port);
  rt.DiscoveryImpl().Add("replicas", port);

  auto ponger = rpc::Make<Ponger>(delay);

  server->Serve(
      rpc::filters::Log(
          std::move(ponger)));

  // Work for some time
  await::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client(std::chrono::milliseconds threshold, matrix::Runtime& rt) {
  timber::log::Logger logger_("Client");

  // Make channel

  auto dialer = rpc::MakeDialer();

  auto balancer = rpc::channels::Random(
      rpc::Dynamic({"replicas"}), std::move(dialer));

  auto attempts_counter = rpc::test::channels::Count(std::move(balancer));

  auto hedger = rpc::channels::Hedge(attempts_counter, {threshold});

  auto stack = rpc::channels::Retry(hedger, rpc::retry::backoff::Fixed(1s));


  // Make calls

  // Counters
  size_t completed_calls = 0;
  size_t slow_calls = 0;

  auto start = rt.WallClock().Now();

  await::fibers::WaitGroup wg;

  static const size_t kCalls = 1000;

  for (size_t i = 0; i < kCalls; ++i) {
    proto::ping::Request ping;

    auto f = rpc::Call("Ponger.Ping")
        .Request(ping)
        .Via(stack)
        .Done()
        .As<proto::ping::Response>();

    wg.Add(std::move(f) | await::futures::Map([&](proto::ping::Response) {
      ++completed_calls;

      auto latency = rt.WallClock().Now() - start;

      if (latency > 3 * threshold) {
        ++slow_calls;
      }
    }));
  }

  wg.Wait();

  const size_t attempts = attempts_counter->Count();

  std::cout << "Completed calls = " << completed_calls << " / " << kCalls << std::endl;
  std::cout << "Total attempts = " << attempts << std::endl;
  std::cout << "Slow calls = " << slow_calls << std::endl;
}

void Sim() {
  matrix::Runtime sim("Hedge");

  sim.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  // Replicas

  // Fast
  for (size_t i = 0; i < 8; ++i) {
    sim.Spawn([&, i]() {
      Server(std::to_string(i), 50ms, sim);
    });
  }

  // Slow

  sim.Spawn([&]() {
    Server("9", 1s, sim);
  });

  sim.Spawn([&]() {
    Server("10", 500ms, sim);
  });

  // Client

  sim.Spawn([&]() {
    Client(100ms, sim);
  });

  sim.Run();
}

int main() {
  Sim();
  return 0;
}
