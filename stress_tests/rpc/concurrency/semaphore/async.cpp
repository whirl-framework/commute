#include <commute/rpc/concurrency/semaphore/async.hpp>

#include <wheels/test/test_framework.hpp>

#include <await/threads/await/future.hpp>
#include <await/threads/sync/wait_group.hpp>
#include <await/executors/impl/fibers/pool.hpp>
#include <await/futures/make/execute.hpp>
#include <await/fibers/await/future.hpp>

using commute::rpc::concurrency::AsyncSemaphore;
using commute::rpc::concurrency::AsyncPermit;

TEST_SUITE(AsyncSemaphore) {
  SIMPLE_TEST(Stress) {
    static const size_t kFibers = 8;
    static const size_t kCsPerFiber = 32768;

    AsyncSemaphore mutex({1, kFibers - 1});
    size_t counter = 0;
    int resource = 0;

    await::executors::fibers::FiberPool pool{4};

    await::threads::WaitGroup wg;
    for (size_t i = 0; i < kFibers; ++i) {
      wg.Add(await::futures::Execute(pool, [&]() {
        for (size_t j = 0; j < kCsPerFiber; ++j) {
          // Lock mutex
          await::fibers::Await(mutex.Acquire()).ExpectOk();

          ++counter;
          ASSERT_FALSE(std::exchange(resource, true));

          resource = false;

          // Unlock mutex
          mutex.Release();
        }
      }));
    }

    wg.Wait();

    std::cout << "Counter = " << counter
              << " (expected = " << kFibers * kCsPerFiber << ")"
              << std::endl;

    ASSERT_EQ(counter, kFibers * kCsPerFiber);

    pool.Stop();
  }
}
