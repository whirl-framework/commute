#include <wheels/test/test_framework.hpp>

#include <commute/rpc/balance/load.hpp>

#include <commute/rpc/balance/load/const.hpp>
#include <commute/rpc/balance/load/inflight.hpp>

#include <commute/rpc/test/channels/echo.hpp>

#include <await/fibers/sync/wait_group.hpp>

#include <carry/new.hpp>

using namespace commute;

TEST_SUITE(LoadMetrics) {
  SIMPLE_TEST(Const) {
    auto load = rpc::balance::load::Const(7);

    auto channel = rpc::test::channels::Echo();
    auto node = load->Measure(std::move(channel));

    ASSERT_EQ(node->Load(), 7);

    auto future = node->Call({
      rpc::Method::Parse("Echo.Echo"),
      "",
      carry::New()
    });

    ASSERT_EQ(node->Load(), 7);
  }

  SIMPLE_TEST(InFlight) {
    auto load = rpc::balance::load::InFlight();

    await::fibers::WaitGroup wg;

    for (size_t i = 0; i < 10; ++i) {

    }
  }
}
