#include <wheels/test/test_framework.hpp>

#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>
#include <commute/rpc/channels/failsafe/hedge.hpp>
#include <commute/rpc/channels/balance/random.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/rpc/resolve/dynamic.hpp>

#include <commute/rpc/test/channels/count.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>

#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>
#include <await/futures/combine/seq/map.hpp>

#include <timber/log/log.hpp>

#include "../../proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

namespace sims {

//////////////////////////////////////////////////////////////////////

class ResourceService :
    public rpc::ServiceBase<ResourceService> {
 public:
  static constexpr const char *kName = "Resource";

  explicit ResourceService(std::chrono::milliseconds delay)
      : delay_(delay),
        logger_(kName) {
  }

  void Ping(const proto::ping::Request & /*request*/,
            Response<proto::ping::Response> & /*response*/) {
    commute::fibers::SleepFor(delay_);
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }

 private:
  std::chrono::milliseconds delay_;

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server(std::string port, std::chrono::milliseconds delay, matrix::Runtime &rt) {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(port);
  rt.DiscoveryImpl().Add("replicas", port);

  auto resource = rpc::Make<ResourceService>(delay);

  server->Serve(
      rpc::filters::Log(
          std::move(resource)));

  // Work for some time
  await::fibers::SleepFor(rt.Timers().Delay(100500s));

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

void Client(std::chrono::milliseconds threshold, matrix::Runtime &rt) {
  timber::log::Logger logger_("Client");

  // Make channel

  auto dialer = rpc::MakeDialer();

  auto balancer = rpc::channels::Random(
      rpc::Dynamic({"replicas"}), std::move(dialer));

  auto attempts_counter = rpc::test::channels::Count(std::move(balancer));

  auto hedger = rpc::channels::Hedge(attempts_counter, {threshold});

  auto retrier = rpc::channels::Retry(hedger, rpc::retry::backoff::Fixed(1s));


  // Make calls

  // Counters
  size_t completed_calls = 0;
  size_t slow_calls = 0;

  auto start = rt.WallClock().Now();

  await::fibers::WaitGroup wg;

  static const size_t kCalls = 1000;

  for (size_t i = 0; i < kCalls; ++i) {
    proto::ping::Request ping;

    auto f = rpc::Call("Resource.Ping")
        .Request(ping)
        .Via(retrier)
        .Done()
        .As<proto::ping::Response>();

    wg.Add(std::move(f) | await::futures::Map([&](proto::ping::Response) {
      ++completed_calls;

      auto latency = rt.WallClock().Now() - start;

      if (latency > 3 * threshold) {
        ++slow_calls;
      }
    }));
  }

  wg.Wait();

  const size_t attempts = attempts_counter->Count();

  std::cout << "Completed calls = " << completed_calls << " / " << kCalls << std::endl;
  std::cout << "Total attempts = " << attempts << std::endl;
  std::cout << "Slow calls = " << slow_calls << std::endl;

  ASSERT_EQ(completed_calls, kCalls);
  ASSERT_LE(attempts, kCalls * 1.3);
  ASSERT_LE(slow_calls, kCalls * 0.1);

  rt.Tester().Checkpoint("ClientDone");
}

void TwoOfTen() {
  matrix::Runtime rt("TwoOfTen");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  // Replicas

  // Fast
  for (size_t i = 0; i < 8; ++i) {
    rt.Spawn([&, i]() {
      Server(std::to_string(i), 50ms, rt);
    });
  }

  // Slow

  rt.Spawn([&]() {
    Server("9", 1s, rt);
  });

  rt.Spawn([&]() {
    Server("10", 500ms, rt);
  });

  // Client

  rt.Spawn([&]() {
    Client(100ms, rt);
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("ClientDone"));
}

}  // namespace sims

TEST_SUITE(Hedge) {
  TEST(Sim, wheels::test::TestOptions().TimeLimit(30s)) {
    sims::TwoOfTen();
  }
}
