#include <wheels/test/test_framework.hpp>

#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/rpc/test/channels/faulty.hpp>
#include <commute/rpc/test/channels/service.hpp>
#include <commute/rpc/test/channels/record.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>
#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/syntax/bang.hpp>

#include <timber/log/log.hpp>

#include <wheels/core/defer.hpp>

#include "../../proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

namespace sims {

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class Adversary : public rpc::test::channels::IAdversary {
 public:
  explicit Adversary(size_t fails)
      : fail_budget_(fails) {
  }

  bool Test(const rpc::Request& /*request*/) override {
    if (fail_budget_ > 0) {
      --fail_budget_;
      return false;
    } else {
      return true;
    }
  }

 private:
  size_t fail_budget_ = 0;
};

//////////////////////////////////////////////////////////////////////

class PingService :
    public rpc::ServiceBase<PingService> {
 public:
  static constexpr const char *kName = "Ping";

  PingService() {
    //
  }

  void Ping(const proto::ping::Request & /*request*/,
           Response<proto::ping::Response> & /*response*/) {
    // No-op
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }
};

//////////////////////////////////////////////////////////////////////

void FixedDelay() {
  matrix::Runtime rt("Fixed");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Info);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 3;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record = rpc::test::channels::Record(std::move(faulty));

    // Retries
    auto retry = rpc::channels::Retry(
        record,
        rpc::retry::backoff::Fixed(2s));

    {
      proto::ping::Request ping;

      auto f = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Done()
          .As<proto::ping::Response>();

      await::Await(std::move(f)).ExpectOk();
    }

    ASSERT_EQ(record->CallCount(), kFails + 1);

    auto ts = record->CallTimestamps();

    ASSERT_EQ(ts[0].TimeSinceEpoch(), 0s);
    ASSERT_EQ(ts[1].TimeSinceEpoch(), 2s);
    ASSERT_EQ(ts[2].TimeSinceEpoch(), 4s);
    ASSERT_EQ(ts[3].TimeSinceEpoch(), 6s);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void ExpBackoff() {
  matrix::Runtime rt("ExpBackoff");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 6;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record = rpc::test::channels::Record(std::move(faulty));

    // Backoff strategy
    auto backoff = rpc::retry::backoff::Exponential({1s, 10s, 2});

    // Retries
    auto retry = rpc::channels::Retry(
        record,
        backoff);

    // Unary call
    {
      proto::ping::Request ping;

      auto f = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Done()
          .As<proto::ping::Response>();

      await::Await(std::move(f)).ExpectOk();
    }

    ASSERT_EQ(record->CallCount(), kFails + 1);

    auto ts = record->CallTimestamps();

    ASSERT_EQ(ts[0].TimeSinceEpoch(), 0s);
    ASSERT_EQ(ts[1].TimeSinceEpoch(), 1s);
    ASSERT_EQ(ts[2].TimeSinceEpoch(), 3s);
    ASSERT_EQ(ts[3].TimeSinceEpoch(), 7s);
    ASSERT_EQ(ts[4].TimeSinceEpoch(), 15s);
    ASSERT_EQ(ts[5].TimeSinceEpoch(), 25s);
    ASSERT_EQ(ts[6].TimeSinceEpoch(), 35s);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void NonBlocking() {
  matrix::Runtime rt("Fixed");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 3;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record = rpc::test::channels::Record(std::move(faulty));

    // Retries
    auto retry = rpc::channels::Retry(
        record,
        rpc::retry::backoff::Fixed(2s));

    {
      proto::ping::Request ping;

      auto before_start = rt.WallClock().Now();

      auto f = !rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Done()
          .As<proto::ping::Response>();

      f.IAmEager();

      auto after_start = rt.WallClock().Now();

      ASSERT_TRUE(before_start == after_start);

      await::Await(std::move(f)).ExpectOk();
    }

    ASSERT_EQ(record->CallCount(), kFails + 1);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void Concurrent() {
  matrix::Runtime rt("Concurrent");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 7;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record1 = rpc::test::channels::Record(faulty);

    auto record2 = rpc::test::channels::Record(faulty);

    // Retries

    auto retry1 = rpc::channels::Retry(
        record1,
        rpc::retry::backoff::Fixed(2s));

    auto retry2 = rpc::channels::Retry(
        record2,
        rpc::retry::backoff::Exponential({1s, 10s, 2}));

    {
      proto::ping::Request ping;

      // Fail budget: 7
      // f1 attempts: 0, 2, 4, 6, 8, ...
      // f2 attempts: 0, 1, 3, 7 (first successful), 15, ...

      auto f1 = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry1)
          .Done()
          .As<proto::ping::Response>();

      auto f2 = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry2)
          .Done()
          .As<proto::ping::Response>();

      auto first = await::futures::FirstOf(std::move(f1), std::move(f2));

      await::Await(std::move(first)).ExpectOk();

      std::cout << "Now -> " << rt.WallClock().Now().TimeSinceEpoch().count() << std::endl;

      ASSERT_EQ(rt.WallClock().Now().TimeSinceEpoch(), 7s);
    }

    await::fibers::SleepFor(rt.Timers().Delay(10s));

    ASSERT_EQ(record2->CallCount(), 4);
    ASSERT_EQ(record1->CallCount(), 4);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void AttemptsLimit() {
  matrix::Runtime rt("AttemptsLimit");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Info);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 6;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record = rpc::test::channels::Record(std::move(faulty));

    // Backoff strategy
    auto backoff = rpc::retry::backoff::Exponential({1s, 10s, 2});

    // Retries
    auto retry = rpc::channels::Retry(
        record,
        backoff);

    // Unary call
    {
      proto::ping::Request ping;

      auto f = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Param<uint64_t>("attempts", 3)
          .Done()
          .As<proto::ping::Response>();

      ASSERT_TRUE(await::Await(std::move(f)).Failed());
    }

    ASSERT_EQ(record->CallCount(), 3);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void Cancellation() {
  matrix::Runtime rt("Retry.Cancel");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Info);

  rt.Spawn([&]() {
    // Setup

    // Ping service
    auto ping = std::make_shared<PingService>();

    // Local invoker
    auto invoke = rpc::test::channels::Service(std::move(ping));

    static const size_t kFails = 100500;

    // Adversary
    auto faulty = rpc::test::channels::Faulty(
        std::move(invoke),
        std::make_unique<Adversary>(kFails));

    // Record calls
    auto record = rpc::test::channels::Record(std::move(faulty));

    // Backoff strategy
    auto backoff = rpc::retry::backoff::Fixed(1s);

    // Retries
    auto retry = rpc::channels::Retry(
        record,
        backoff);

    // Unary call
    {
      proto::ping::Request ping;

      auto f = !rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Done()
          .As<proto::ping::Response>();

      f.IAmEager();

      await::fibers::SleepFor(rt.Timers().Delay(100s));

      std::move(f).RequestCancel();
    }

    await::fibers::SleepFor(rt.Timers().Delay(100s));

    ASSERT_LE(record->CallCount(), 111);

    rt.Tester().Checkpoint("Done");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("Done"));
}

//////////////////////////////////////////////////////////////////////

void Random() {
  matrix::Runtime rt("Random");

  rt.Spawn([&]() {
    // Server

    timber::log::Logger logger_("Server");

    auto server = rpc::MakeServer(kServerPort);

    auto ping = rpc::Make<PingService>();

    // Add concurrency limit
    auto semaphore = rpc::filters::Semaphore(
        std::move(ping),
        rpc::filters::SemaphoreParams()
            .ConcurrencyLimit(2)
            .QueueLimit(1));

    // Logging
    auto root = rpc::filters::Log(std::move(semaphore));

    server->Serve(std::move(root));

    // Work for some time
    await::fibers::SleepFor(rt.Timers().Delay(100500s));

    server->Shutdown();

    TIMBER_LOG_INFO("Server stopped");

    rt.Tester().Checkpoint("ServerDone");
  });

  rt.Spawn([&]() {
    // Client

    timber::log::Logger logger_("Client");

    auto client = rpc::MakeDialer();

    auto socket = client->Dial(kServerPort);

    // Add automatic retries

    auto retry = rpc::channels::Retry(
        std::move(socket),
        rpc::retry::backoff::WithJitter(
            rpc::retry::backoff::Exponential({1s, 10s, 2}),
            rpc::retry::jitter::Full(rt.Random())));

    await::fibers::WaitGroup calls;

    static const size_t kCalls = 10;

    for (size_t i = 0; i < kCalls; ++i) {
      proto::ping::Request ping;

      auto call = rpc::Call("Ping.Ping")
          .Request(ping)
          .Via(retry)
          .Done()
          .As<proto::ping::Response>();

      calls.Add(std::move(call));
    }

    calls.Wait();

    rt.Tester().Checkpoint("ClientDone");
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("ServerDone"));
  ASSERT_TRUE(rt.Tester().Reached("ClientDone"));
}

//////////////////////////////////////////////////////////////////////

}  // namespace sims

TEST_SUITE(Retries) {
  SIMPLE_TEST(FixedDelay) {
    sims::FixedDelay();
  }

  SIMPLE_TEST(ExpBackoff) {
    sims::ExpBackoff();
  }

  SIMPLE_TEST(NonBlocking) {
    sims::NonBlocking();
  }

  SIMPLE_TEST(Concurrent) {
    sims::Concurrent();
  }

  SIMPLE_TEST(AttemptsLimit) {
    sims::AttemptsLimit();
  }

  SIMPLE_TEST(Cancellation) {
    sims::Cancellation();
  }

  SIMPLE_TEST(Random) {
    sims::Random();
  }
}
