#include <wheels/test/test_framework.hpp>

#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/timeout.hpp>
#include <commute/rpc/retry/backoff/exponential.hpp>

#include <commute/rpc/filters/route.hpp>
#include <commute/rpc/filters/deadline.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>

#include <await/await.hpp>

#include <timber/log/log.hpp>

#include <wheels/core/defer.hpp>

#include "../../proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

namespace timeout {

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

class SlowpokeService :
    public rpc::ServiceBase<SlowpokeService> {
 public:
  static constexpr const char* kName = "Slowpoke";

  explicit SlowpokeService(matrix::Tester& tester)
      : tester_(tester) {
  }

  void Ping(const proto::ping::Request& /*request*/,
          Response<proto::ping::Response>& /*response*/) {
    wheels::Defer defer([this]() {
      tester_.Checkpoint("SlowpokeHandlerCancelled");
    });

    tester_.Checkpoint("SlowpokeHandlerStarted");

    while (true) {
      commute::fibers::SleepFor(1s);  // <- Cancellation checkpoint
    }

    FAIL_TEST("Unreachable");
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }

 private:
  matrix::Tester& tester_;
};

//////////////////////////////////////////////////////////////////////

class ProxyService :
    public commute::rpc::ServiceBase<ProxyService> {
 public:
  static constexpr const char* kName = "Proxy";

  ProxyService(matrix::Tester& tester)
        : tester_(tester), backend_(MakeBackendChannel()) {
  }

  void Ping(const proto::ping::Request& request,
          Response<proto::ping::Response>& response) {
    wheels::Defer defer([this]() {
      tester_.Checkpoint("ProxyHandlerCancelled");
    });

    tester_.Checkpoint("ProxyHandlerStarted");

    auto future = rpc::Call("Slowpoke.Ping")
        .Request(request)
        .Via(backend_)
        .Done()
        .As<proto::ping::Response>();

    auto result = await::Await(std::move(future));

    FAIL_TEST("Unreachable");

    response.Set(std::move(result));
  }

 private:
  commute::rpc::IChannelPtr MakeBackendChannel() {
    auto client = rpc::MakeDialer();

    return client->Dial(kServerPort);
  }

  void RegisterMethods() {
    COMMUTE_RPC_REGISTER_METHOD(Ping);
  }

 private:
  matrix::Tester& tester_;
  commute::rpc::IChannelPtr backend_;
};

//////////////////////////////////////////////////////////////////////

// Server

void Server(matrix::Tester& tester) {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  {
    auto slowpoke = rpc::Make<SlowpokeService>(tester);
    auto proxy = rpc::Make<ProxyService>(tester);

    auto router = rpc::filters::Route();

    router->Add(std::move(slowpoke));
    router->Add(std::move(proxy));

    auto deadlines = rpc::filters::DeadLine(std::move(router));

    auto root = rpc::filters::Log(std::move(deadlines));

    server->Serve(std::move(root));
  }

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

// Client

void Client(matrix::Tester& tester) {
  timber::log::Logger logger_("Client");

  auto client = rpc::MakeDialer();

  auto socket = client->Dial(kServerPort);

  auto with_timeout = rpc::channels::Timeout(std::move(socket));

  proto::ping::Request ping;

  // Make a call
  auto future = rpc::Call("Proxy.Ping")
      .Request(ping)
      .Via(with_timeout)
      .Timeout(5s)
      .Done()
      .As<proto::ping::Response>();

  auto result = await::Await(std::move(future));

  ASSERT_TRUE(result.Failed());

  TIMBER_LOG_INFO("Request failed: {}", result.Error().Describe());

  TIMBER_LOG_INFO("Client completed");

  tester.Checkpoint("ClientDone");
}

//////////////////////////////////////////////////////////////////////

int Sim() {
  matrix::Runtime rt("Timeout");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Info);

  rt.Spawn([&]() {
    Server(rt.Tester());
  });

  rt.Spawn([&]() {
    Client(rt.Tester());
  });

  rt.Run();

  auto& tester = rt.Tester();

  ASSERT_TRUE(tester.Reached("ProxyHandlerStarted"));
  ASSERT_TRUE(tester.Reached("SlowpokeHandlerStarted"));

  ASSERT_TRUE(tester.Reached("ProxyHandlerCancelled"));
  ASSERT_TRUE(tester.Reached("SlowpokeHandlerCancelled"));

  ASSERT_TRUE(tester.Reached("ClientDone"));

  return 0;
}

}  // namespace timeout

TEST_SUITE(Timeout) {
  SIMPLE_TEST(Sim) {
    timeout::Sim();
  }
}
