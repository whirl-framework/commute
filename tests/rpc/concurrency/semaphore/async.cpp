#include <commute/concurrency/semaphore/async.hpp>

#include <wheels/test/test_framework.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

using commute::concurrency::AsyncSemaphore;
using commute::concurrency::AsyncPermit;

TEST_SUITE(AsyncSemaphore) {
  SIMPLE_TEST(Acquire) {
    AsyncSemaphore sema({1, 123});

    auto f = !sema.Acquire();

    await::Await(std::move(f)).ExpectOk();

    sema.Release();
  }

  SIMPLE_TEST(Wait) {
    AsyncSemaphore sema({1, 1});

    auto f1 = !sema.Acquire();
    auto f2 = !sema.Acquire();

    await::Await(std::move(f1)).ExpectOk();

    ASSERT_FALSE(f2.HasOutput());

    sema.Release();

    ASSERT_TRUE(f2.HasOutput());
    (*std::move(f2)).ExpectOk();

    sema.Release();
  }

  SIMPLE_TEST(Fail) {
    AsyncSemaphore sema({1, 1});

    auto f1 = !sema.Acquire();
    auto f2 = !sema.Acquire();
    auto f3 = !sema.Acquire();

    ASSERT_TRUE(f1.HasOutput());
    ASSERT_FALSE(f2.HasOutput());

    ASSERT_TRUE(f3.HasOutput());
    ASSERT_TRUE((*std::move(f3)).Failed());

    sema.Release();
    ASSERT_TRUE(f2.HasOutput());

    sema.Release();
    sema.Release();
  }

  SIMPLE_TEST(Fifo) {
    AsyncSemaphore sema({1, 5});

    using Permit = await::futures::EagerFuture<fallible::Status>;

    std::vector<Permit> permits;
    for (size_t i = 0; i < 6; ++i) {
      permits.push_back(!sema.Acquire());
    }

    for (size_t i = 0; i < permits.size(); ++i) {
      ASSERT_TRUE(permits[i].HasOutput());

      for (size_t j = i + 1; j < permits.size(); ++j) {
        ASSERT_FALSE(permits[j].HasOutput());
      }

      (*std::move(permits[i])).ExpectOk();

      sema.Release();
    }
  }
}
