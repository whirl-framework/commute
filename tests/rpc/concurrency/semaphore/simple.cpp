#include <commute/concurrency/semaphore/simple.hpp>

#include <wheels/test/test_framework.hpp>

using commute::concurrency::Semaphore;

TEST_SUITE(SimpleSemaphore) {
  SIMPLE_TEST(Acquire) {
    Semaphore sema(2);

    // -1
    ASSERT_TRUE(sema.TryAcquire());
    // -1
    ASSERT_TRUE(sema.TryAcquire());

    ASSERT_FALSE(sema.TryAcquire());
    ASSERT_FALSE(sema.TryAcquire());

    // +1
    sema.Release();

    // -1
    ASSERT_TRUE(sema.TryAcquire());
    ASSERT_FALSE(sema.TryAcquire());

    // +1
    sema.Release();
    // +1
    sema.Release();

    // -1
    ASSERT_TRUE(sema.TryAcquire());
    // -1
    ASSERT_TRUE(sema.TryAcquire());

    sema.Release();
    sema.Release();
  }
}
