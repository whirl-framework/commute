#include <wheels/test/test_framework.hpp>

#include <commute/rt/matrix/runtime.hpp>

#include <commute/rpc/server/server.hpp>
#include <commute/rpc/server/service.hpp>

#include <commute/rpc/client/dialer.hpp>
#include <commute/rpc/client/call.hpp>

#include <commute/rpc/channels/failsafe/retry.hpp>

#include <commute/rpc/retry/backoff/exponential.hpp>
#include <commute/rpc/retry/backoff/fixed.hpp>
#include <commute/rpc/retry/backoff/jitter.hpp>

#include <commute/rpc/filters/semaphore.hpp>
#include <commute/rpc/filters/log.hpp>

#include <commute/rpc/core/server/invoker.hpp>

#include <commute/rpc/test/channels/record.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/await.hpp>

#include <timber/log/log.hpp>

#include "../proto/cpp/ping.pb.h"

#include <chrono>

using namespace commute;
using namespace std::chrono_literals;

namespace semaphore {

//////////////////////////////////////////////////////////////////////

static const std::string kServerPort = "42";

//////////////////////////////////////////////////////////////////////

static const size_t kCalls = 15;

static const size_t kConcurrencyLimit = 3;
static const size_t kQueueLimit = 2;

//////////////////////////////////////////////////////////////////////

class ResourceService :
    public rpc::ServiceBase<ResourceService> {
 public:
  static constexpr const char *kName = "Resource";

  ResourceService(std::atomic<size_t>& counter)
      : counter_(counter),
        logger_(kName) {
  }

  void Use(const proto::ping::Request & /*request*/,
           Response<proto::ping::Response> & /*response*/) {
    DoWork();
  }

 private:
  void DoWork() {
    size_t load = ++load_;
    ASSERT_TRUE(load <= kConcurrencyLimit);

    commute::fibers::SleepFor(1s);

    ++counter_;

    --load_;
  }

 protected:
  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Use);
  }

 private:
  std::atomic<size_t>& counter_;

  std::atomic<size_t> load_{0};

  timber::log::Logger logger_;
};

//////////////////////////////////////////////////////////////////////

void Server() {
  timber::log::Logger logger_("Server");

  auto server = rpc::MakeServer(kServerPort);

  std::atomic<size_t> counter{0};

  auto resource = rpc::Make<ResourceService>(counter);

  // Add concurrency limit
  auto semaphore = rpc::filters::Semaphore(
      std::move(resource),
      rpc::filters::SemaphoreParams()
          .ConcurrencyLimit(kConcurrencyLimit)
          .QueueLimit(kQueueLimit));

  auto log = rpc::filters::Log(std::move(semaphore));

  server->Serve(std::move(log));

  // Work for some time
  commute::fibers::SleepFor(100500s);

  server->Shutdown();

  std::cout << "Resource use count = " << counter.load() << std::endl;
  ASSERT_EQ(counter.load(), kCalls)

  TIMBER_LOG_INFO("Server stopped");
}

//////////////////////////////////////////////////////////////////////

// Backoff strategy for retries

rpc::retry::IBackoffPtr Backoff() {
  return rpc::retry::backoff::Fixed(2s);
}

void Client(matrix::Runtime& rt) {
  timber::log::Logger logger_("Client", rt.LogBackend());

  auto client = rpc::MakeDialer();

  auto socket = client->Dial(kServerPort);

  auto recorder = rpc::test::channels::Record(std::move(socket));

  // Add automatic retries

  auto retry = rpc::channels::Retry(
      recorder,
      Backoff());

  await::fibers::WaitGroup calls;

  for (size_t i = 0; i < kCalls; ++i) {
    proto::ping::Request ping;

    auto call = rpc::Call("Resource.Use")
        .Request(ping)
        .Via(retry)
        .Done()
        .As<proto::ping::Response>();

    calls.Add(std::move(call));
  }

  calls.Wait();

  const size_t attempts = recorder->CallCount();

  std::cout << "Attempts = " << attempts << std::endl;
  ASSERT_EQ(attempts, 30);

  rt.Tester().Checkpoint("ClientDone");
}

int Sim() {
  matrix::Runtime rt("Semaphore");

  rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Debug);

  rt.Spawn([]() {
    Server();
  }).Spawn([&]() {
    Client(rt);
  });

  rt.Run();

  ASSERT_TRUE(rt.Tester().Reached("ClientDone"));

  return 0;
}

}  // namespace semaphore

TEST_SUITE(Semaphore) {
  SIMPLE_TEST(Sim) {
    semaphore::Sim();
  }
}
