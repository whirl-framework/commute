#include <commute/rpc/retry/jitter/equal.hpp>
#include <commute/rpc/retry/jitter/full.hpp>

#include <wheels/test/test_framework.hpp>

//////////////////////////////////////////////////////////////////////

struct MinGenerator : snowflake::random::IGenerator {
  uint64_t Jitter(uint64_t /*max*/) override {
    return 0;
  }

  size_t Choice(size_t /*size*/) override {
    return 0;
  }
};

//////////////////////////////////////////////////////////////////////

struct MaxGenerator : snowflake::random::IGenerator {
  uint64_t Jitter(uint64_t max) override {
    return max;
  }

  size_t Choice(size_t /*size*/) override {
    return 0;
  }
};

//////////////////////////////////////////////////////////////////////

using namespace commute;

TEST_SUITE(Jitter) {
  SIMPLE_TEST(Full) {
    {
      MaxGenerator random;

      auto jitter = rpc::retry::jitter::Full(random);

      ASSERT_EQ(jitter->Add(100ms), 100ms);
    }

    {
      MinGenerator random;

      auto jitter = rpc::retry::jitter::Full(random);

      ASSERT_EQ(jitter->Add(100ms), 0ms);
    }
  }

  SIMPLE_TEST(Equal) {
    {
      MaxGenerator random;

      auto jitter = rpc::retry::jitter::Equal(random);

      ASSERT_EQ(jitter->Add(100ms), 100ms);
    }

    {
      MinGenerator random;

      auto jitter = rpc::retry::jitter::Equal(random);

      ASSERT_EQ(jitter->Add(100ms), 50ms);
      ASSERT_EQ(jitter->Add(256ms), 128ms);
    }
  }
}
