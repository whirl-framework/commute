#include <wheels/test/test_framework.hpp>

#include <commute/rt/matrix/runtime.hpp>

#include <commute/concurrency/fibers/sleep_for.hpp>

#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sched/sleep_for.hpp>

using namespace commute;

using namespace std::chrono_literals;

TEST_SUITE(Matrix) {
  SIMPLE_TEST(Spawn) {
    matrix::Runtime rt("Test");

    rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

    rt.Spawn([&rt]() {
      for (size_t i = 0; i < 4; ++i) {
        await::fibers::Yield();
      }

      rt.Tester().Checkpoint("Done");
    });

    rt.Run();

    ASSERT_TRUE(rt.Tester().Reached("Done"));
  }

  SIMPLE_TEST(Time) {
    matrix::Runtime rt("Test");

    rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

    rt.Spawn([&rt]() {
      auto now = rt.WallClock().Now();

      commute::fibers::SleepFor(3s);

      auto elapsed = rt.WallClock().Now() - now;

      ASSERT_EQ(elapsed, 3s);

      rt.Tester().Checkpoint("Done");
    });

    rt.Run();

    ASSERT_TRUE(rt.Tester().Reached("Done"));
  }

  SIMPLE_TEST(Ids) {
    matrix::Runtime rt("Test");

    rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

    rt.Spawn([&rt]() {
      auto id1 = rt.Ids().GenerateId();
      auto id2 = rt.Ids().GenerateId();
      auto id3 = rt.Ids().GenerateId();

      ASSERT_NE(id1, id2);
      ASSERT_NE(id2, id3);
      ASSERT_NE(id3, id1);
    });

    rt.Run();
  }

  SIMPLE_TEST(Discovery) {
    matrix::Runtime rt("Test");

    rt.LogBackendImpl()
      .SetDefaultLevel(timber::log::Level::Off);

    // https://en.wikipedia.org/wiki/Kang_and_Kodos
    rt.DiscoveryImpl().Add("aliens", "kang");
    rt.DiscoveryImpl().Add("aliens", "kodos");

    rt.Spawn([&rt]() {
      auto addrs = rt.Discovery().Lookup({"aliens"});

      ASSERT_TRUE(addrs.size() == 2);

      ASSERT_EQ(addrs[0], "kang");
      ASSERT_EQ(addrs[1], "kodos");
    });

    rt.Run();
  }
}
